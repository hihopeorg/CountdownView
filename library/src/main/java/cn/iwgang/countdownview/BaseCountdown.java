package cn.iwgang.countdownview;


import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * Base Countdown
 * Created by iWgang on 16/6/18.
 * https://github.com/iwgang/CountdownView
 */
public class BaseCountdown {
    private static final String DEFAULT_SUFFIX = ":";
    private static final float DEFAULT_SUFFIX_LR_MARGIN = 3;

    public int mDay, mHour, mMinute, mSecond, mMillisecond;
    public boolean isShowDay, isShowHour, isShowMinute, isShowSecond, isShowMillisecond;

    public boolean isConvertDaysToHours;
    public boolean mHasSetIsShowDay, mHasSetIsShowHour;

    protected Context mContext;

    public String getmSuffixDay() {
        return mSuffixDay;
    }

    public String getmSuffixHour() {
        return mSuffixHour;
    }

    public String getmSuffixMinute() {
        return mSuffixMinute;
    }

    public String getmSuffixSecond() {
        return mSuffixSecond;
    }

    public String getmSuffixMillisecond() {
        return mSuffixMillisecond;
    }

    protected String mSuffix, mSuffixDay, mSuffixHour, mSuffixMinute, mSuffixSecond, mSuffixMillisecond;
    protected float mSuffixDayTextWidth, mSuffixHourTextWidth, mSuffixMinuteTextWidth, mSuffixSecondTextWidth, mSuffixMillisecondTextWidth;
    protected boolean isDayLargeNinetyNine;
    protected Paint mTimeTextPaint, mSuffixTextPaint, mMeasureHourWidthPaint;
    protected float mLeftPaddingSize;
    protected float mSuffixDayLeftMargin, mSuffixDayRightMargin;
    protected float mSuffixSecondLeftMargin, mSuffixSecondRightMargin;
    protected float mSuffixHourLeftMargin, mSuffixHourRightMargin;
    protected float mSuffixMinuteLeftMargin, mSuffixMinuteRightMargin;
    protected float mSuffixMillisecondLeftMargin;
    protected float mSuffixDayTextBaseline, mSuffixHourTextBaseline, mSuffixMinuteTextBaseline, mSuffixSecondTextBaseline, mSuffixMillisecondTextBaseline;
    protected float mTimeTextWidth, mTimeTextHeight, mTimeTextBottom;
    protected String mSuffixGravity;

    private boolean hasSetSuffixDay, hasSetSuffixHour, hasSetSuffixMinute, hasSetSuffixSecond, hasSetSuffixMillisecond;
    private boolean hasCustomSomeSuffix;
    private float mSuffixLRMargin;
    private int mTimeTextColor;
    private float mTimeTextSize;
    private boolean isTimeTextBold;
    private int mSuffixTextColor;
    private float mSuffixTextSize;
    private boolean isSuffixTextBold;
    private float mDayTimeTextWidth, mHourTimeTextWidth;
    private float mTimeTextBaseline;

    private float mTempSuffixDayLeftMargin, mTempSuffixDayRightMargin;
    private float mTempSuffixHourLeftMargin, mTempSuffixHourRightMargin;
    private float mTempSuffixMinuteLeftMargin, mTempSuffixMinuteRightMargin;
    private float mTempSuffixSecondLeftMargin, mTempSuffixSecondRightMargin;
    private float mTempSuffixMillisecondLeftMargin;
    private String mTempSuffixMinute, mTempSuffixSecond;

    public void initStyleAttr(Context context, AttrSet ta) {
        mContext = context;
        isTimeTextBold = ta.getAttr("isTimeTextBold").isPresent() ? ta.getAttr("isTimeTextBold").get().getBoolValue() : false;
        mTimeTextSize = ta.getAttr("timeTextSize").isPresent() ? ta.getAttr("timeTextSize").get().getDimensionValue() : Utils.fp2px(mContext, 12);
        mTimeTextColor = ta.getAttr("timeTextColor").isPresent() ? ta.getAttr("timeTextColor").get().getColorValue().getValue() : 0xFF000000;
        isShowDay = ta.getAttr("isShowDay").isPresent() ? ta.getAttr("isShowDay").get().getBoolValue() : false;
        isShowHour = ta.getAttr("isShowHour").isPresent() ? ta.getAttr("isShowHour").get().getBoolValue() : false;
        isShowMinute = ta.getAttr("isShowMinute").isPresent() ? ta.getAttr("isShowMinute").get().getBoolValue() : true;
        isShowSecond = ta.getAttr("isShowSecond").isPresent() ? ta.getAttr("isShowSecond").get().getBoolValue() : true;
        isShowMillisecond = ta.getAttr("isShowMillisecond").isPresent() ? ta.getAttr("isShowMillisecond").get().getBoolValue() : false;
        if (ta.getAttr("isHideTimeBackground").isPresent() ? ta.getAttr("isHideTimeBackground").get().getBoolValue() : true) {
            isConvertDaysToHours = ta.getAttr("isConvertDaysToHours").isPresent() ? ta.getAttr("isConvertDaysToHours").get().getBoolValue() : false;
        }
        isSuffixTextBold = ta.getAttr("isSuffixTextBold").isPresent() ? ta.getAttr("isSuffixTextBold").get().getBoolValue() : false;
        mSuffixTextSize = ta.getAttr("suffixTextSize").isPresent() ? ta.getAttr("suffixTextSize").get().getDimensionValue() : Utils.fp2px(mContext, 12);
        mSuffixTextColor = ta.getAttr("suffixTextColor").isPresent() ? ta.getAttr("suffixTextColor").get().getColorValue().getValue() : 0xFF000000;
        mSuffix = ta.getAttr("suffix").isPresent() ? ta.getAttr("suffix").get().getStringValue() : "";
        mSuffixDay = ta.getAttr("suffixDay").isPresent() ? ta.getAttr("suffixDay").get().getStringValue() : "";
        mSuffixHour = ta.getAttr("suffixHour").isPresent() ? ta.getAttr("suffixHour").get().getStringValue() : "";
        mSuffixMinute = ta.getAttr("suffixMinute").isPresent() ? ta.getAttr("suffixMinute").get().getStringValue() : "";
        mSuffixSecond = ta.getAttr("suffixSecond").isPresent() ? ta.getAttr("suffixSecond").get().getStringValue() : "";
        mSuffixMillisecond = ta.getAttr("suffixMillisecond").isPresent() ? ta.getAttr("suffixMillisecond").get().getStringValue() : "";

        mSuffixGravity = ta.getAttr("suffixGravity").isPresent() ? ta.getAttr("suffixGravity").get().getStringValue() : DynamicConfig.SuffixGravity.CENTER;

        mSuffixLRMargin = ta.getAttr("suffixLRMargin").isPresent() ? ta.getAttr("suffixLRMargin").get().getDimensionValue() : -1;
        mSuffixDayLeftMargin = ta.getAttr("suffixDayLeftMargin").isPresent() ? ta.getAttr("suffixDayLeftMargin").get().getDimensionValue() : -1;
        mSuffixDayRightMargin = ta.getAttr("suffixDayRightMargin").isPresent() ? ta.getAttr("suffixDayRightMargin").get().getDimensionValue() : -1;
        mSuffixHourLeftMargin = ta.getAttr("suffixHourLeftMargin").isPresent() ? ta.getAttr("suffixHourLeftMargin").get().getDimensionValue() : -1;
        mSuffixHourRightMargin = ta.getAttr("suffixHourRightMargin").isPresent() ? ta.getAttr("suffixHourRightMargin").get().getDimensionValue() : -1;
        mSuffixMinuteLeftMargin = ta.getAttr("suffixMinuteLeftMargin").isPresent() ? ta.getAttr("suffixMinuteLeftMargin").get().getDimensionValue() : -1;
        mSuffixMinuteRightMargin = ta.getAttr("suffixMinuteRightMargin").isPresent() ? ta.getAttr("suffixMinuteRightMargin").get().getDimensionValue() : -1;
        mSuffixSecondLeftMargin = ta.getAttr("suffixSecondLeftMargin").isPresent() ? ta.getAttr("suffixSecondLeftMargin").get().getDimensionValue() : -1;
        mSuffixSecondRightMargin = ta.getAttr("suffixSecondRightMargin").isPresent() ? ta.getAttr("suffixSecondRightMargin").get().getDimensionValue() : -1;
        mSuffixMillisecondLeftMargin = ta.getAttr("suffixMillisecondLeftMargin").isPresent() ? ta.getAttr("suffixMillisecondLeftMargin").get().getDimensionValue() : -1;

        mHasSetIsShowDay = ta.getAttr("isShowDay").isPresent();
        mHasSetIsShowHour = ta.getAttr("isShowHour").isPresent();

        initTempSuffixMargin();

        // time validate
        if (!isShowDay && !isShowHour && !isShowMinute) isShowSecond = true;
        if (!isShowSecond) isShowMillisecond = false;
    }

    public void initialize() {
        initSuffixBase();

        // initialize
        initPaint();
        initSuffix();

        // regular time data
        // pick one of two (minute and second)
//        if (!isShowMinute && !isShowSecond) isShowSecond = true;
        if (!isShowSecond) isShowMillisecond = false;

        initTimeTextBaseInfo();
    }

    private void initSuffixBase() {
        hasSetSuffixDay = !isEmpty(mSuffixDay);
        hasSetSuffixHour = !isEmpty(mSuffixHour);
        hasSetSuffixMinute = !isEmpty(mSuffixMinute);
        hasSetSuffixSecond = !isEmpty(mSuffixSecond);
        hasSetSuffixMillisecond = !isEmpty(mSuffixMillisecond);

        if ((isShowDay && hasSetSuffixDay)
                || (isShowHour && hasSetSuffixHour)
                || (isShowMinute && hasSetSuffixMinute)
                || (isShowSecond && hasSetSuffixSecond)
                || (isShowMillisecond && hasSetSuffixMillisecond)) {
            hasCustomSomeSuffix = true;
        }

        mTempSuffixMinute = mSuffixMinute;
        mTempSuffixSecond = mSuffixSecond;
    }

    private void initTempSuffixMargin() {
        // temporarily saved suffix left and right margins
        mTempSuffixDayLeftMargin = mSuffixDayLeftMargin;
        mTempSuffixDayRightMargin = mSuffixDayRightMargin;
        mTempSuffixHourLeftMargin = mSuffixHourLeftMargin;
        mTempSuffixHourRightMargin = mSuffixHourRightMargin;
        mTempSuffixMinuteLeftMargin = mSuffixMinuteLeftMargin;
        mTempSuffixMinuteRightMargin = mSuffixMinuteRightMargin;
        mTempSuffixSecondLeftMargin = mSuffixSecondLeftMargin;
        mTempSuffixSecondRightMargin = mSuffixSecondRightMargin;
        mTempSuffixMillisecondLeftMargin = mSuffixMillisecondLeftMargin;
    }

    /**
     * initialize Paint
     */
    public void initPaint() {
        // time text
        mTimeTextPaint = new Paint();
        mTimeTextPaint.setColor(new Color(mTimeTextColor));
        mTimeTextPaint.setTextAlign(TextAlignment.CENTER);
        mTimeTextPaint.setTextSize((int) mTimeTextSize);
        if (isTimeTextBold) {
            mTimeTextPaint.setFakeBoldText(true);
        }

        // suffix text
        mSuffixTextPaint = new Paint();
        mSuffixTextPaint.setColor(new Color(mSuffixTextColor));
        mSuffixTextPaint.setTextSize((int) mSuffixTextSize);
        if (isSuffixTextBold) {
            mSuffixTextPaint.setFakeBoldText(true);
        }

        mMeasureHourWidthPaint = new Paint();
        mMeasureHourWidthPaint.setTextSize((int) mTimeTextSize);
        if (isTimeTextBold) {
            mMeasureHourWidthPaint.setFakeBoldText(true);
        }
    }

    private void initSuffix() {
        boolean isSuffixNull = true;
        float mSuffixTextWidth = 0;
        float mDefSuffixTextWidth = mSuffixTextPaint.measureText(DEFAULT_SUFFIX);

        if (!isEmpty(mSuffix)) {
            isSuffixNull = false;
            mSuffixTextWidth = mSuffixTextPaint.measureText(mSuffix);
        }

        if (isShowDay) {
            if (hasSetSuffixDay) {
                mSuffixDayTextWidth = mSuffixTextPaint.measureText(mSuffixDay);
            } else {
                if (!isSuffixNull) {
                    mSuffixDay = mSuffix;
                    mSuffixDayTextWidth = mSuffixTextWidth;
                } else if (!hasCustomSomeSuffix) {
                    mSuffixDay = DEFAULT_SUFFIX;
                    mSuffixDayTextWidth = mDefSuffixTextWidth;
                }
            }
        } else {
            mSuffixDayTextWidth = 0;
        }

        if (isShowHour) {
            if (hasSetSuffixHour) {
                mSuffixHourTextWidth = mSuffixTextPaint.measureText(mSuffixHour);
            } else {
                if (!isSuffixNull) {
                    mSuffixHour = mSuffix;
                    mSuffixHourTextWidth = mSuffixTextWidth;
                } else if (!hasCustomSomeSuffix) {
                    mSuffixHour = DEFAULT_SUFFIX;
                    mSuffixHourTextWidth = mDefSuffixTextWidth;
                }
            }
        } else {
            mSuffixHourTextWidth = 0;
        }

        if (isShowMinute) {
            if (hasSetSuffixMinute) {
                mSuffixMinuteTextWidth = mSuffixTextPaint.measureText(mSuffixMinute);
            } else if (isShowSecond) {
                if (!isSuffixNull) {
                    mSuffixMinute = mSuffix;
                    mSuffixMinuteTextWidth = mSuffixTextWidth;
                } else if (!hasCustomSomeSuffix) {
                    mSuffixMinute = DEFAULT_SUFFIX;
                    mSuffixMinuteTextWidth = mDefSuffixTextWidth;
                }
            } else {
                mSuffixMinuteTextWidth = 0;
            }
        } else {
            mSuffixMinuteTextWidth = 0;
        }

        if (isShowSecond) {
            if (hasSetSuffixSecond) {
                mSuffixSecondTextWidth = mSuffixTextPaint.measureText(mSuffixSecond);
            } else if (isShowMillisecond) {
                if (!isSuffixNull) {
                    mSuffixSecond = mSuffix;
                    mSuffixSecondTextWidth = mSuffixTextWidth;
                } else if (!hasCustomSomeSuffix) {
                    mSuffixSecond = DEFAULT_SUFFIX;
                    mSuffixSecondTextWidth = mDefSuffixTextWidth;
                }
            } else {
                mSuffixSecondTextWidth = 0;
            }
        } else {
            mSuffixSecondTextWidth = 0;
        }

        if (isShowMillisecond && hasCustomSomeSuffix && hasSetSuffixMillisecond) {
            mSuffixMillisecondTextWidth = mSuffixTextPaint.measureText(mSuffixMillisecond);
        } else {
            mSuffixMillisecondTextWidth = 0;
        }

        initSuffixMargin();
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * initialize suffix margin
     */
    private void initSuffixMargin() {
        int defSuffixLRMargin = Utils.vp2px(mContext, DEFAULT_SUFFIX_LR_MARGIN);
        boolean isSuffixLRMarginNull = true;

        if (mSuffixLRMargin >= 0) {
            isSuffixLRMarginNull = false;
        }

        if (isShowDay && mSuffixDayTextWidth > 0) {
            if (mSuffixDayLeftMargin < 0) {
                if (!isSuffixLRMarginNull) {
                    mSuffixDayLeftMargin = mSuffixLRMargin;
                } else {
                    mSuffixDayLeftMargin = defSuffixLRMargin;
                }
            }

            if (mSuffixDayRightMargin < 0) {
                if (!isSuffixLRMarginNull) {
                    mSuffixDayRightMargin = mSuffixLRMargin;
                } else {
                    mSuffixDayRightMargin = defSuffixLRMargin;
                }
            }
        } else {
            mSuffixDayLeftMargin = 0;
            mSuffixDayRightMargin = 0;
        }

        if (isShowHour && mSuffixHourTextWidth > 0) {
            if (mSuffixHourLeftMargin < 0) {
                if (!isSuffixLRMarginNull) {
                    mSuffixHourLeftMargin = mSuffixLRMargin;
                } else {
                    mSuffixHourLeftMargin = defSuffixLRMargin;
                }
            }

            if (mSuffixHourRightMargin < 0) {
                if (!isSuffixLRMarginNull) {
                    mSuffixHourRightMargin = mSuffixLRMargin;
                } else {
                    mSuffixHourRightMargin = defSuffixLRMargin;
                }
            }
        } else {
            mSuffixHourLeftMargin = 0;
            mSuffixHourRightMargin = 0;
        }

        if (isShowMinute && mSuffixMinuteTextWidth > 0) {
            if (mSuffixMinuteLeftMargin < 0) {
                if (!isSuffixLRMarginNull) {
                    mSuffixMinuteLeftMargin = mSuffixLRMargin;
                } else {
                    mSuffixMinuteLeftMargin = defSuffixLRMargin;
                }
            }

            if (isShowSecond) {
                if (mSuffixMinuteRightMargin < 0) {
                    if (!isSuffixLRMarginNull) {
                        mSuffixMinuteRightMargin = mSuffixLRMargin;
                    } else {
                        mSuffixMinuteRightMargin = defSuffixLRMargin;
                    }
                }
            } else {
                mSuffixMinuteRightMargin = 0;
            }
        } else {
            mSuffixMinuteLeftMargin = 0;
            mSuffixMinuteRightMargin = 0;
        }

        if (isShowSecond) {
            if (mSuffixSecondTextWidth > 0) {
                if (mSuffixSecondLeftMargin < 0) {
                    if (!isSuffixLRMarginNull) {
                        mSuffixSecondLeftMargin = mSuffixLRMargin;
                    } else {
                        mSuffixSecondLeftMargin = defSuffixLRMargin;
                    }
                }

                if (isShowMillisecond) {
                    if (mSuffixSecondRightMargin < 0) {
                        if (!isSuffixLRMarginNull) {
                            mSuffixSecondRightMargin = mSuffixLRMargin;
                        } else {
                            mSuffixSecondRightMargin = defSuffixLRMargin;
                        }
                    }
                } else {
                    mSuffixSecondRightMargin = 0;
                }
            } else {
                mSuffixSecondLeftMargin = 0;
                mSuffixSecondRightMargin = 0;
            }

            if (isShowMillisecond && mSuffixMillisecondTextWidth > 0) {
                if (mSuffixMillisecondLeftMargin < 0) {
                    if (!isSuffixLRMarginNull) {
                        mSuffixMillisecondLeftMargin = mSuffixLRMargin;
                    } else {
                        mSuffixMillisecondLeftMargin = defSuffixLRMargin;
                    }
                }
            } else {
                mSuffixMillisecondLeftMargin = 0;
            }
        } else {
            mSuffixSecondLeftMargin = 0;
            mSuffixSecondRightMargin = 0;
            mSuffixMillisecondLeftMargin = 0;
        }
    }

    protected void initTimeTextBaseInfo() {
        // initialize time text width and height
        Rect rect;
        rect = mTimeTextPaint.getTextBounds("00");
        mTimeTextWidth = rect.getWidth();
        mTimeTextHeight = rect.getHeight();
        mTimeTextBottom = rect.bottom;
    }

    private void initTimeTextBaseline(int viewHeight, int viewPaddingTop, int viewPaddingBottom) {
        if (viewPaddingTop == viewPaddingBottom) {
            // center
            mTimeTextBaseline = viewHeight / 2 + mTimeTextHeight / 2 - mTimeTextBottom;
        } else {
            // padding top
            mTimeTextBaseline = viewHeight - (viewHeight - viewPaddingTop) + mTimeTextHeight - mTimeTextBottom;
        }

        if (isShowDay && mSuffixDayTextWidth > 0) {
            mSuffixDayTextBaseline = getSuffixTextBaseLine(mSuffixDay);
        }

        if (isShowHour && mSuffixHourTextWidth > 0) {
            mSuffixHourTextBaseline = getSuffixTextBaseLine(mSuffixHour);
        }

        if (isShowMinute && mSuffixMinuteTextWidth > 0) {
            mSuffixMinuteTextBaseline = getSuffixTextBaseLine(mSuffixMinute);
        }

        if (mSuffixSecondTextWidth > 0) {
            mSuffixSecondTextBaseline = getSuffixTextBaseLine(mSuffixSecond);
        }

        if (isShowMillisecond && mSuffixMillisecondTextWidth > 0) {
            mSuffixMillisecondTextBaseline = getSuffixTextBaseLine(mSuffixMillisecond);
        }
    }

    private float getSuffixTextBaseLine(String suffixText) {
        Rect tempRect;
        tempRect = mSuffixTextPaint.getTextBounds(suffixText);

        float ret;
        switch (mSuffixGravity) {
            case DynamicConfig.SuffixGravity.TOP:
                // top
                ret = mTimeTextBaseline - mTimeTextHeight - tempRect.top;
                break;
            case DynamicConfig.SuffixGravity.BOTTOM:
                // bottom
                ret = mTimeTextBaseline - tempRect.bottom;
                break;
            default:
                ret = mTimeTextBaseline - mTimeTextHeight / 2 + tempRect.getHeight() / 2;
                break;
        }

        return ret;
    }

    protected final float getAllContentWidthBase(float timeWidth) {
        float width = (mSuffixDayTextWidth + mSuffixHourTextWidth + mSuffixMinuteTextWidth + mSuffixSecondTextWidth + mSuffixMillisecondTextWidth);
        width += (mSuffixDayLeftMargin + mSuffixDayRightMargin + mSuffixHourLeftMargin + mSuffixHourRightMargin
                + mSuffixMinuteLeftMargin + mSuffixMinuteRightMargin + mSuffixSecondLeftMargin + mSuffixSecondRightMargin + mSuffixMillisecondLeftMargin);

        if (isConvertDaysToHours) {
            width += getDayAndHourContentWidth();
        } else if (isShowHour) {
            width += timeWidth;
        }

        if (isShowMinute) {
            width += timeWidth;
        }

        if (isShowSecond) {
            width += timeWidth;
        }

        if (isShowMillisecond) {
            width += timeWidth;
        }
        return width;
    }

    private float getDayAndHourContentWidth() {
        float width = 0;

        Rect tempRect;

        if (isShowDay) {
            String tempDay = Utils.formatNum(mDay);
            tempRect = mTimeTextPaint.getTextBounds(tempDay);
            mDayTimeTextWidth = tempRect.getWidth();

            width += mDayTimeTextWidth;
        }

        if (isShowHour) {
            String tempHour = Utils.formatNum(mHour);
            tempRect = mMeasureHourWidthPaint.getTextBounds(tempHour);
            mHourTimeTextWidth = tempRect.getWidth();

            width += mHourTimeTextWidth;
        }

        return width;
    }

    /**
     * get all view width
     *
     * @return all view width
     */
    public int getAllContentWidth() {
        float width = getAllContentWidthBase(mTimeTextWidth);

        if (!isConvertDaysToHours && isShowDay) {
            if (isDayLargeNinetyNine) {
                Rect rect;
                String tempDay = String.valueOf(mDay);
                rect = mTimeTextPaint.getTextBounds(tempDay);
                mDayTimeTextWidth = rect.getWidth();
                width += mDayTimeTextWidth;
            } else {
                mDayTimeTextWidth = mTimeTextWidth;
                width += mTimeTextWidth;
            }
        }

        return (int) Math.ceil(width);
    }

    public int getAllContentHeight() {
        return (int) mTimeTextHeight;
    }

    public void onMeasure(Component v, int viewWidth, int viewHeight, int allContentWidth, int allContentHeight) {
        initTimeTextBaseline(viewHeight, v.getPaddingTop(), v.getPaddingBottom());
        mLeftPaddingSize = v.getPaddingLeft() == v.getPaddingRight() ? (viewWidth - allContentWidth) / 2 : v.getPaddingLeft();
    }

    public void onDraw(Canvas canvas) {
        // not background
        float mHourLeft;
        float mMinuteLeft;
        float mSecondLeft;

        if (isShowDay) {
            // draw day text
            canvas.drawText(mTimeTextPaint, Utils.formatNum(mDay), mLeftPaddingSize + mDayTimeTextWidth / 2, mTimeTextBaseline);
            if (mSuffixDayTextWidth > 0) {
                // draw day suffix
                canvas.drawText(mSuffixTextPaint, mSuffixDay, mLeftPaddingSize + mDayTimeTextWidth + mSuffixDayLeftMargin, mSuffixDayTextBaseline);
            }

            // hour left point
            mHourLeft = mLeftPaddingSize + mDayTimeTextWidth + mSuffixDayTextWidth + mSuffixDayLeftMargin + mSuffixDayRightMargin;
        } else {
            // hour left point
            mHourLeft = mLeftPaddingSize;
        }

        if (isShowHour) {
            // draw hour text
            float curTimeTextWidth = isConvertDaysToHours ? mHourTimeTextWidth : mTimeTextWidth;
            canvas.drawText(mTimeTextPaint, Utils.formatNum(mHour), mHourLeft + curTimeTextWidth / 2, mTimeTextBaseline);
            if (mSuffixHourTextWidth > 0) {
                // draw hour suffix
                canvas.drawText(mSuffixTextPaint, mSuffixHour, mHourLeft + curTimeTextWidth + mSuffixHourLeftMargin, mSuffixHourTextBaseline);
            }

            // minute left point
            mMinuteLeft = mHourLeft + curTimeTextWidth + mSuffixHourTextWidth + mSuffixHourLeftMargin + mSuffixHourRightMargin;
        } else {
            // minute left point
            mMinuteLeft = mHourLeft;
        }

        if (isShowMinute) {
            // draw minute text
            canvas.drawText(mTimeTextPaint, Utils.formatNum(mMinute), mMinuteLeft + mTimeTextWidth / 2, mTimeTextBaseline);
            if (mSuffixMinuteTextWidth > 0) {
                // draw minute suffix
                canvas.drawText(mSuffixTextPaint, mSuffixMinute, mMinuteLeft + mTimeTextWidth + mSuffixMinuteLeftMargin, mSuffixMinuteTextBaseline);
            }

            // second left point
            mSecondLeft = mMinuteLeft + mTimeTextWidth + mSuffixMinuteTextWidth + mSuffixMinuteLeftMargin + mSuffixMinuteRightMargin;
        } else {
            // second left point
            mSecondLeft = mMinuteLeft;
        }

        if (isShowSecond) {
            // draw second text
            canvas.drawText(mTimeTextPaint, Utils.formatNum(mSecond), mSecondLeft + mTimeTextWidth / 2, mTimeTextBaseline);
            if (mSuffixSecondTextWidth > 0) {
                // draw second suffix
                canvas.drawText(mSuffixTextPaint, mSuffixSecond, mSecondLeft + mTimeTextWidth + mSuffixSecondLeftMargin, mSuffixSecondTextBaseline);
            }

            if (isShowMillisecond) {
                // millisecond left point
                float mMillisecondLeft = mSecondLeft + mTimeTextWidth + mSuffixSecondTextWidth + mSuffixSecondLeftMargin + mSuffixSecondRightMargin;
                // draw millisecond text
                canvas.drawText(mTimeTextPaint, Utils.formatMillisecond(mMillisecond), mMillisecondLeft + mTimeTextWidth / 2, mTimeTextBaseline);
                if (mSuffixMillisecondTextWidth > 0) {
                    // draw millisecond suffix
                    canvas.drawText(mSuffixTextPaint, mSuffixMillisecond, mMillisecondLeft + mTimeTextWidth + mSuffixMillisecondLeftMargin, mSuffixMillisecondTextBaseline);
                }
            }
        }
    }

    public boolean refTimeShow(boolean isShowDay, boolean isShowHour, boolean isShowMinute, boolean isShowSecond, boolean isShowMillisecond) {
        if (!isShowSecond) isShowMillisecond = false;

        boolean isModCountdownInterval = false;
        if (this.isShowDay != isShowDay) {
            this.isShowDay = isShowDay;
            // reset day margins
            if (isShowDay) {
                mSuffixDayLeftMargin = mTempSuffixDayLeftMargin;
                mSuffixDayRightMargin = mTempSuffixDayRightMargin;
            }
        }

        if (this.isShowHour != isShowHour) {
            this.isShowHour = isShowHour;
            // reset hour margins
            if (isShowHour) {
                mSuffixHourLeftMargin = mTempSuffixHourLeftMargin;
                mSuffixHourRightMargin = mTempSuffixHourRightMargin;
            }
        }

        if (this.isShowMinute != isShowMinute) {
            this.isShowMinute = isShowMinute;
            // reset minute margins
            if (isShowMinute) {
                mSuffixMinuteLeftMargin = mTempSuffixMinuteLeftMargin;
                mSuffixMinuteRightMargin = mTempSuffixMinuteRightMargin;
                mSuffixMinute = mTempSuffixMinute;
            }
        }

        if (this.isShowSecond != isShowSecond) {
            this.isShowSecond = isShowSecond;
            isModCountdownInterval = true;

            // reset second margins
            if (isShowSecond) {
                mSuffixSecondLeftMargin = mTempSuffixSecondLeftMargin;
                mSuffixSecondRightMargin = mTempSuffixSecondRightMargin;
                mSuffixSecond = mTempSuffixSecond;
            } else {
                mSuffixMinute = mTempSuffixMinute;
            }

            mSuffixMinuteLeftMargin = mTempSuffixMinuteLeftMargin;
            mSuffixMinuteRightMargin = mTempSuffixMinuteRightMargin;
        }

        if (this.isShowMillisecond != isShowMillisecond) {
            this.isShowMillisecond = isShowMillisecond;
            isModCountdownInterval = true;

            // reset millisecond margins
            if (isShowMillisecond) {
                mSuffixMillisecondLeftMargin = mTempSuffixMillisecondLeftMargin;
            } else {
                mSuffixSecond = mTempSuffixSecond;
            }

            mSuffixSecondLeftMargin = mTempSuffixSecondLeftMargin;
            mSuffixSecondRightMargin = mTempSuffixSecondRightMargin;
        }

        return isModCountdownInterval;
    }

    public boolean handlerAutoShowTime() {
        boolean isReLayout = false;
        if (!mHasSetIsShowDay) {
            if (!isShowDay && mDay > 0) {
                // auto show day
                // judgement auto show hour
                if (!mHasSetIsShowHour) {
                    refTimeShow(true, true, isShowMinute, isShowSecond, isShowMillisecond);
                } else {
                    refTimeShow(true, isShowHour, isShowMinute, isShowSecond, isShowMillisecond);
                }
                isReLayout = true;
            } else if (isShowDay && mDay == 0) {
                // auto hide day
                refTimeShow(false, isShowHour, isShowMinute, isShowSecond, isShowMillisecond);
                isReLayout = true;
            } else {
                if (!mHasSetIsShowHour) {
                    if (!isShowHour && (mDay > 0 || mHour > 0)) {
                        // auto show hour
                        refTimeShow(isShowDay, true, isShowMinute, isShowSecond, isShowMillisecond);
                        isReLayout = true;
                    } else if (isShowHour && mDay == 0 && mHour == 0) {
                        // auto hide hour
                        refTimeShow(false, false, isShowMinute, isShowSecond, isShowMillisecond);
                        isReLayout = true;
                    }
                }
            }
        } else {
            if (!mHasSetIsShowHour) {
                if (!isShowHour && (mDay > 0 || mHour > 0)) {
                    // auto show hour
                    refTimeShow(isShowDay, true, isShowMinute, isShowSecond, isShowMillisecond);
                    isReLayout = true;
                } else if (isShowHour && mDay == 0 && mHour == 0) {
                    // auto hide hour
                    refTimeShow(isShowDay, false, isShowMinute, isShowSecond, isShowMillisecond);
                    isReLayout = true;
                }
            }
        }

        return isReLayout;
    }

    public boolean handlerDayLargeNinetyNine() {
        boolean isReLayout = false;
        if (isShowDay) {
            // handler large ninety nine
            if (!isDayLargeNinetyNine && mDay > 99) {
                isDayLargeNinetyNine = true;
                isReLayout = true;
            } else if (isDayLargeNinetyNine && mDay <= 99) {
                isDayLargeNinetyNine = false;
                isReLayout = true;
            }
        }
        return isReLayout;
    }

    public void setTimes(int day, int hour, int minute, int second, int millisecond) {
        mDay = day;
        mHour = hour;
        mMinute = minute;
        mSecond = second;
        mMillisecond = millisecond;
    }

    public void reLayout() {
        initSuffix();
        initTimeTextBaseInfo();
    }

    public void setTimeTextSize(float textSize) {
        if (textSize > 0) {
            mTimeTextSize = Utils.fp2px(mContext, textSize);
            mTimeTextPaint.setTextSize((int) mTimeTextSize);
        }
    }

    public float getmTimeTextSize() {
        return mTimeTextSize;
    }

    public void setTimeTextColor(int textColor) {
        mTimeTextColor = textColor;
        mTimeTextPaint.setColor(new Color(mTimeTextColor));
    }

    public int getmTimeTextColor() {
        return mTimeTextColor;
    }

    public void setTimeTextBold(boolean isBold) {
        isTimeTextBold = isBold;
        mTimeTextPaint.setFakeBoldText(isTimeTextBold);
    }

    public boolean getisTimeTextBold() {
        return isTimeTextBold;
    }

    public void setSuffixTextSize(float textSize) {
        if (textSize > 0) {
            mSuffixTextSize = Utils.fp2px(mContext, textSize);
            mSuffixTextPaint.setTextSize((int) mSuffixTextSize);
        }
    }

    public float getmSuffixTextSize() {
        return mSuffixTextSize;
    }

    public void setSuffixTextColor(int textColor) {
        mSuffixTextColor = textColor;
        mSuffixTextPaint.setColor(new Color(mSuffixTextColor));
    }

    public int getmSuffixTextColor() {
        return mSuffixTextColor;
    }

    public void setSuffixTextBold(boolean isBold) {
        isSuffixTextBold = isBold;
        mSuffixTextPaint.setFakeBoldText(isSuffixTextBold);
    }

    public boolean getisSuffixTextBold() {
        return isSuffixTextBold;
    }

    public void setSuffix(String suffix) {
        mSuffix = suffix;
        setSuffix(mSuffix, mSuffix, mSuffix, mSuffix, mSuffix);
    }

    public String getmSuffix() {
        return mSuffix;
    }

    public boolean setConvertDaysToHours(boolean isConvertDaysToHours) {
        if (this.isConvertDaysToHours == isConvertDaysToHours) return false;
        this.isConvertDaysToHours = isConvertDaysToHours;
        return true;
    }

    public boolean setSuffix(String suffixDay, String suffixHour, String suffixMinute, String suffixSecond, String suffixMillisecond) {
        boolean isRef = false;

        if (null != suffixDay) {
            mSuffixDay = suffixDay;
            isRef = true;
        }

        if (null != suffixHour) {
            mSuffixHour = suffixHour;
            isRef = true;
        }

        if (null != suffixMinute) {
            mSuffixMinute = suffixMinute;
            isRef = true;
        }

        if (null != suffixSecond) {
            mSuffixSecond = suffixSecond;
            isRef = true;
        }

        if (null != suffixMillisecond) {
            mSuffixMillisecond = suffixMillisecond;
            isRef = true;
        }

        if (isRef) initSuffixBase();

        return isRef;
    }

    public void setSuffixLRMargin(float suffixLRMargin) {
        mSuffixLRMargin = Utils.vp2px(mContext, suffixLRMargin);
        setSuffixMargin(mSuffixDayLeftMargin, mSuffixDayLeftMargin,
                mSuffixDayLeftMargin, mSuffixDayLeftMargin,
                mSuffixDayLeftMargin, mSuffixDayLeftMargin,
                mSuffixDayLeftMargin, mSuffixDayLeftMargin,
                mSuffixDayLeftMargin);
    }

    public float getmSuffixLRMargin() {
        return mSuffixLRMargin;
    }

    public boolean setSuffixMargin(Float suffixDayMarginL, Float suffixDayMarginR,
                                   Float suffixHourMarginL, Float suffixHourMarginR,
                                   Float suffixMinuteMarginL, Float suffixMinuteMarginR,
                                   Float suffixSecondMarginL, Float suffixSecondMarginR,
                                   Float suffixMillisecondMarginL) {
        boolean isRef = false;

        if (null != suffixDayMarginL) {
            mSuffixDayLeftMargin = Utils.vp2px(mContext, suffixDayMarginL);
            isRef = true;
        }
        if (null != suffixDayMarginR) {
            mSuffixDayRightMargin = Utils.vp2px(mContext, suffixDayMarginR);
            isRef = true;
        }

        if (null != suffixHourMarginL) {
            mSuffixHourLeftMargin = Utils.vp2px(mContext, suffixHourMarginL);
            isRef = true;
        }
        if (null != suffixHourMarginR) {
            mSuffixHourRightMargin = Utils.vp2px(mContext, suffixHourMarginR);
            isRef = true;
        }

        if (null != suffixMinuteMarginL) {
            mSuffixMinuteLeftMargin = Utils.vp2px(mContext, suffixMinuteMarginL);
            isRef = true;
        }
        if (null != suffixMinuteMarginR) {
            mSuffixMinuteRightMargin = Utils.vp2px(mContext, suffixMinuteMarginR);
            isRef = true;
        }

        if (null != suffixSecondMarginL) {
            mSuffixSecondLeftMargin = Utils.vp2px(mContext, suffixSecondMarginL);
            isRef = true;
        }
        if (null != suffixSecondMarginR) {
            mSuffixSecondRightMargin = Utils.vp2px(mContext, suffixSecondMarginR);
            isRef = true;
        }

        if (null != suffixMillisecondMarginL) {
            mSuffixMillisecondLeftMargin = Utils.vp2px(mContext, suffixMillisecondMarginL);
            isRef = true;
        }


        if (isRef) initTempSuffixMargin();

        return isRef;
    }

    public void setSuffixGravity(String suffixGravity) {
        mSuffixGravity = suffixGravity;
    }

    public String getmSuffixGravity() {
        return mSuffixGravity;
    }

    private boolean isEmpty(CharSequence str) {
        return str == null || str.length() == 0;
    }

}
