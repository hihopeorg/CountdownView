package cn.iwgang.countdownview;


import ohos.agp.components.AttrHelper;
import ohos.app.Context;

/**
 * Utils
 * Created by iWgang on 16/6/19.
 * https://github.com/iwgang/CountdownView
 */
public final class Utils {

    public static int vp2px(Context context, float vpValue) {
        return AttrHelper.vp2px(vpValue, context);
    }

    public static float fp2px(Context context, float fpValue) {
        return AttrHelper.fp2px(fpValue, context);
    }

    public static String formatNum(int time) {
        return time < 10 ? "0" + time : String.valueOf(time);
    }

    public static String formatMillisecond(int millisecond) {
        String retMillisecondStr;

        if (millisecond > 99) {
            retMillisecondStr = String.valueOf(millisecond / 10);
        } else if (millisecond <= 9) {
            retMillisecondStr = "0" + millisecond;
        } else {
            retMillisecondStr = String.valueOf(millisecond);
        }

        return retMillisecondStr;
    }

}
