package cn.iwgang.countdownview;


import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

/**
 * Background Countdown
 * Created by iWgang on 16/6/19.
 * https://github.com/iwgang/CountdownView
 */
public class BackgroundCountdown extends BaseCountdown {
    private static final float DEFAULT_TIME_BG_DIVISION_LINE_SIZE = 0.5f;
    private static final float DEFAULT_TIME_BG_BORDER_SIZE = 1f;

    private boolean isDrawBg;
    private boolean isShowTimeBgDivisionLine;
    private int mTimeBgDivisionLineColor;
    private float mTimeBgDivisionLineSize;
    private float mTimeBgRadius;
    private float mTimeBgSize;
    private int mTimeBgColor;
    private Paint mTimeBgPaint, mTimeBgBorderPaint, mTimeBgDivisionLinePaint;
    private float mDefSetTimeBgSize;
    private float mDayTimeBgWidth;
    private RectFloat mDayBgRectF, mHourBgRectF, mMinuteBgRectF, mSecondBgRectF, mMillisecondBgRectF;
    private RectFloat mDayBgBorderRectF, mHourBgBorderRectF, mMinuteBgBorderRectF, mSecondBgBorderRectF, mMillisecondBgBorderRectF;
    private float mTimeBgDivisionLineYPos;
    private float mTimeTextBaseY;
    private boolean isShowTimeBgBorder;
    private float mTimeBgBorderSize;
    private float mTimeBgBorderRadius;
    private int mTimeBgBorderColor;

    @Override
    public void initStyleAttr(Context context, AttrSet ta) {
        super.initStyleAttr(context, ta);

        mTimeBgColor = ta.getAttr("timeBgColor").isPresent() ? ta.getAttr("timeBgColor").get().getColorValue().getValue() : 0xFF444444;
        mTimeBgRadius = ta.getAttr("timeBgRadius").isPresent() ? ta.getAttr("timeBgRadius").get().getDimensionValue() : 0;
        isShowTimeBgDivisionLine = ta.getAttr("isShowTimeBgDivisionLine").isPresent() ? ta.getAttr("isShowTimeBgDivisionLine").get().getBoolValue() : true;
        mTimeBgDivisionLineColor = ta.getAttr("timeBgDivisionLineColor").isPresent() ? ta.getAttr("timeBgDivisionLineColor").get().getColorValue().getValue() : Color.getIntColor("#30FFFFFF");
        mTimeBgDivisionLineSize = ta.getAttr("timeBgDivisionLineSize").isPresent() ? ta.getAttr("timeBgDivisionLineSize").get().getDimensionValue() : Utils.vp2px(context, DEFAULT_TIME_BG_DIVISION_LINE_SIZE);
        mTimeBgSize = ta.getAttr("timeBgSize").isPresent() ? ta.getAttr("timeBgSize").get().getDimensionValue() : 0;
        mDefSetTimeBgSize = mTimeBgSize;
        mTimeBgBorderSize = ta.getAttr("timeBgBorderSize").isPresent() ? ta.getAttr("timeBgBorderSize").get().getDimensionValue() : Utils.vp2px(context, DEFAULT_TIME_BG_BORDER_SIZE);
        mTimeBgBorderRadius = ta.getAttr("timeBgBorderRadius").isPresent() ? ta.getAttr("timeBgBorderRadius").get().getDimensionValue() : 0;
        mTimeBgBorderColor = ta.getAttr("timeBgBorderColor").isPresent() ? ta.getAttr("timeBgBorderColor").get().getColorValue().getValue() : 0xFF000000;
        isShowTimeBgBorder = ta.getAttr("isShowTimeBgBorder").isPresent() ? ta.getAttr("isShowTimeBgBorder").get().getBoolValue() : false;

        isDrawBg = ta.getAttr("timeBgColor").isPresent() || !isShowTimeBgBorder;
    }

    public int getmTimeBgColor() {
        return mTimeBgColor;
    }

    public float getmTimeBgSize() {
        return mTimeBgSize;
    }

    @Override
    public void initPaint() {
        super.initPaint();
        // time background
        mTimeBgPaint = new Paint();
        mTimeBgPaint.setStyle(Paint.Style.FILL_STYLE);
        mTimeBgPaint.setColor(new Color(mTimeBgColor));

        // time background border
        if (isShowTimeBgBorder) {
            initTimeBgBorderPaint();
        }

        // time background division line
        if (isShowTimeBgDivisionLine) {
            initTimeTextBgDivisionLinePaint();
        }
    }

    private void initTimeBgBorderPaint() {
        if (null != mTimeBgBorderPaint) return;

        mTimeBgBorderPaint = new Paint();
        mTimeBgBorderPaint.setColor(new Color(mTimeBgBorderColor));
        if (!isDrawBg) {
            mTimeBgBorderPaint.setStrokeWidth(mTimeBgBorderSize);
            mTimeBgBorderPaint.setStyle(Paint.Style.STROKE_STYLE);
        }
    }

    private void initTimeTextBgDivisionLinePaint() {
        if (null != mTimeBgDivisionLinePaint) return;

        mTimeBgDivisionLinePaint = new Paint();
        mTimeBgDivisionLinePaint.setColor(new Color(mTimeBgDivisionLineColor));
        mTimeBgDivisionLinePaint.setStrokeWidth(mTimeBgDivisionLineSize);
    }

    @Override
    protected void initTimeTextBaseInfo() {
        super.initTimeTextBaseInfo();

        if (mDefSetTimeBgSize == 0 || mTimeBgSize < mTimeTextWidth) {
            mTimeBgSize = mTimeTextWidth + (Utils.vp2px(mContext, 2) * 4);
        }
    }

    /**
     * initialize time initialize rectF
     */
    private void initTimeBgRect(float topPaddingSize) {
        float mHourLeft;
        float mMinuteLeft;
        float mSecondLeft;
        boolean isInitHasBackgroundTextBaseY = false;

        if (isShowDay) {
            // initialize day background and border rectF
            if (isShowTimeBgBorder) {
                mDayBgBorderRectF = new RectFloat(mLeftPaddingSize, topPaddingSize, mLeftPaddingSize + mDayTimeBgWidth + (mTimeBgBorderSize * 2), topPaddingSize + mTimeBgSize + (mTimeBgBorderSize * 2));
                mDayBgRectF = new RectFloat(mLeftPaddingSize + mTimeBgBorderSize, topPaddingSize + mTimeBgBorderSize, mLeftPaddingSize + mDayTimeBgWidth + mTimeBgBorderSize, topPaddingSize + mTimeBgSize + mTimeBgBorderSize);
            } else {
                mDayBgRectF = new RectFloat(mLeftPaddingSize, topPaddingSize, mLeftPaddingSize + mDayTimeBgWidth, topPaddingSize + mTimeBgSize);
            }
            // hour left point
            mHourLeft = mLeftPaddingSize + mDayTimeBgWidth + mSuffixDayTextWidth + mSuffixDayLeftMargin + mSuffixDayRightMargin + (mTimeBgBorderSize * 2);

            if (!isShowHour && !isShowMinute && !isShowSecond) {
                isInitHasBackgroundTextBaseY = true;
                initHasBackgroundTextBaseY(mDayBgRectF);
            }
        } else {
            // hour left point
            mHourLeft = mLeftPaddingSize;
        }

        if (isShowHour) {
            // initialize hour background border rectF
            if (isShowTimeBgBorder) {
                mHourBgBorderRectF = new RectFloat(mHourLeft, topPaddingSize, mHourLeft + mTimeBgSize + (mTimeBgBorderSize * 2), topPaddingSize + mTimeBgSize + (mTimeBgBorderSize * 2));
                mHourBgRectF = new RectFloat(mHourLeft + mTimeBgBorderSize, topPaddingSize + mTimeBgBorderSize, mHourLeft + mTimeBgSize + mTimeBgBorderSize, topPaddingSize + mTimeBgSize + mTimeBgBorderSize);
            } else {
                mHourBgRectF = new RectFloat(mHourLeft, topPaddingSize, mHourLeft + mTimeBgSize, topPaddingSize + mTimeBgSize);
            }
            // minute left point
            mMinuteLeft = mHourLeft + mTimeBgSize + mSuffixHourTextWidth + mSuffixHourLeftMargin + mSuffixHourRightMargin + (mTimeBgBorderSize * 2);

            if (!isInitHasBackgroundTextBaseY) {
                isInitHasBackgroundTextBaseY = true;
                initHasBackgroundTextBaseY(mHourBgRectF);
            }
        } else {
            // minute left point
            mMinuteLeft = mHourLeft;
        }

        if (isShowMinute) {
            // initialize minute background border rectF
            if (isShowTimeBgBorder) {
                mMinuteBgBorderRectF = new RectFloat(mMinuteLeft, topPaddingSize, mMinuteLeft + mTimeBgSize + (mTimeBgBorderSize * 2), topPaddingSize + mTimeBgSize + (mTimeBgBorderSize * 2));
                mMinuteBgRectF = new RectFloat(mMinuteLeft + mTimeBgBorderSize, topPaddingSize + mTimeBgBorderSize, mMinuteLeft + mTimeBgSize + mTimeBgBorderSize, topPaddingSize + mTimeBgSize + mTimeBgBorderSize);
            } else {
                mMinuteBgRectF = new RectFloat(mMinuteLeft, topPaddingSize, mMinuteLeft + mTimeBgSize, topPaddingSize + mTimeBgSize);
            }
            // second left point
            mSecondLeft = mMinuteLeft + mTimeBgSize + mSuffixMinuteTextWidth + mSuffixMinuteLeftMargin + mSuffixMinuteRightMargin + (mTimeBgBorderSize * 2);

            if (!isInitHasBackgroundTextBaseY) {
                isInitHasBackgroundTextBaseY = true;
                initHasBackgroundTextBaseY(mMinuteBgRectF);
            }
        } else {
            // second left point
            mSecondLeft = mMinuteLeft;
        }

        if (isShowSecond) {
            // initialize second background border rectF
            if (isShowTimeBgBorder) {
                mSecondBgBorderRectF = new RectFloat(mSecondLeft, topPaddingSize, mSecondLeft + mTimeBgSize + (mTimeBgBorderSize * 2), topPaddingSize + mTimeBgSize + (mTimeBgBorderSize * 2));
                mSecondBgRectF = new RectFloat(mSecondLeft + mTimeBgBorderSize, topPaddingSize + mTimeBgBorderSize, mSecondLeft + mTimeBgSize + mTimeBgBorderSize, topPaddingSize + mTimeBgSize + mTimeBgBorderSize);
            } else {
                mSecondBgRectF = new RectFloat(mSecondLeft, topPaddingSize, mSecondLeft + mTimeBgSize, topPaddingSize + mTimeBgSize);
            }

            if (isShowMillisecond) {
                // millisecond left point
                float mMillisecondLeft = mSecondLeft + mTimeBgSize + mSuffixSecondTextWidth + mSuffixSecondLeftMargin + mSuffixSecondRightMargin + (mTimeBgBorderSize * 2);

                // initialize millisecond background border rectF
                if (isShowTimeBgBorder) {
                    mMillisecondBgBorderRectF = new RectFloat(mMillisecondLeft, topPaddingSize, mMillisecondLeft + mTimeBgSize + (mTimeBgBorderSize * 2), topPaddingSize + mTimeBgSize + (mTimeBgBorderSize * 2));
                    mMillisecondBgRectF = new RectFloat(mMillisecondLeft + mTimeBgBorderSize, topPaddingSize + mTimeBgBorderSize, mMillisecondLeft + mTimeBgSize + mTimeBgBorderSize, topPaddingSize + mTimeBgSize + mTimeBgBorderSize);
                } else {
                    mMillisecondBgRectF = new RectFloat(mMillisecondLeft, topPaddingSize, mMillisecondLeft + mTimeBgSize, topPaddingSize + mTimeBgSize);
                }
            }

            if (!isInitHasBackgroundTextBaseY) {
                initHasBackgroundTextBaseY(mSecondBgRectF);
            }
        }
    }

    private float getSuffixTextBaseLine(String suffixText, float topPaddingSize) {
        Rect tempRect;
        tempRect = mSuffixTextPaint.getTextBounds(suffixText);

        float ret;
        switch (mSuffixGravity) {
            case DynamicConfig.SuffixGravity.TOP:
                // top
                ret = topPaddingSize - tempRect.top;
                break;
            case DynamicConfig.SuffixGravity.BOTTOM:
                // bottom
                ret = topPaddingSize + mTimeBgSize - tempRect.bottom + (mTimeBgBorderSize * 2);
                break;
            default:
                ret = topPaddingSize + mTimeBgSize - mTimeBgSize / 2 + tempRect.getHeight() / 2 + mTimeBgBorderSize;
                break;
        }

        return ret;
    }

    private void initHasBackgroundTextBaseY(RectFloat rectF) {
        // time text baseline
        Paint.FontMetrics timeFontMetrics = mTimeTextPaint.getFontMetrics();
        mTimeTextBaseY = rectF.top + (rectF.bottom - rectF.top - timeFontMetrics.bottom + timeFontMetrics.top) / 2 - timeFontMetrics.top - mTimeTextBottom;
        // initialize background division line y point
        mTimeBgDivisionLineYPos = rectF.getVerticalCenter() + (mTimeBgDivisionLineSize == Utils.vp2px(mContext, DEFAULT_TIME_BG_DIVISION_LINE_SIZE) ? mTimeBgDivisionLineSize : mTimeBgDivisionLineSize / 2);
    }

    /**
     * initialize time text baseline
     * and
     * time background top padding
     */
    private float initTimeTextBaselineAndTimeBgTopPadding(int viewHeight, int viewPaddingTop, int viewPaddingBottom, int contentAllHeight) {
        float topPaddingSize;
        if (viewPaddingTop == viewPaddingBottom) {
            // center
            topPaddingSize = (viewHeight - contentAllHeight) / 2;
        } else {
            // padding top
            topPaddingSize = viewPaddingTop;
        }

        if (isShowDay && mSuffixDayTextWidth > 0) {
            mSuffixDayTextBaseline = getSuffixTextBaseLine(mSuffixDay, topPaddingSize);
        }

        if (isShowHour && mSuffixHourTextWidth > 0) {
            mSuffixHourTextBaseline = getSuffixTextBaseLine(mSuffixHour, topPaddingSize);
        }

        if (isShowMinute && mSuffixMinuteTextWidth > 0) {
            mSuffixMinuteTextBaseline = getSuffixTextBaseLine(mSuffixMinute, topPaddingSize);
        }

        if (mSuffixSecondTextWidth > 0) {
            mSuffixSecondTextBaseline = getSuffixTextBaseLine(mSuffixSecond, topPaddingSize);
        }

        if (isShowMillisecond && mSuffixMillisecondTextWidth > 0) {
            mSuffixMillisecondTextBaseline = getSuffixTextBaseLine(mSuffixMillisecond, topPaddingSize);
        }

        return topPaddingSize;
    }

    @Override
    public int getAllContentWidth() {
        float width = getAllContentWidthBase(mTimeBgSize + (mTimeBgBorderSize * 2));

        if (isShowDay) {
            if (isDayLargeNinetyNine) {
                Rect rect;
                String tempDay = String.valueOf(mDay);
                rect = mTimeTextPaint.getTextBounds(tempDay);
                mDayTimeBgWidth = rect.getWidth() + (Utils.vp2px(mContext, 2) * 4);
                width += mDayTimeBgWidth;
            } else {
                mDayTimeBgWidth = mTimeBgSize;
                width += mTimeBgSize;
            }

            width += (mTimeBgBorderSize * 2);
        }

        return (int) Math.ceil(width);
    }

    @Override
    public int getAllContentHeight() {
        return (int) (mTimeBgSize + (mTimeBgBorderSize * 2));
    }

    @Override
    public void onMeasure(Component v, int viewWidth, int viewHeight, int allContentWidth, int allContentHeight) {
        float retTopPaddingSize = initTimeTextBaselineAndTimeBgTopPadding(viewHeight, v.getPaddingTop(), v.getPaddingBottom(), allContentHeight);
        mLeftPaddingSize = v.getPaddingLeft() == v.getPaddingRight() ? (viewWidth - allContentWidth) / 2 : v.getPaddingLeft();
        initTimeBgRect(retTopPaddingSize);
    }

    @Override
    public void onDraw(Canvas canvas) {
        // show background
        float mHourLeft;
        float mMinuteLeft;
        float mSecondLeft;

        if (isShowDay) {
            // draw day background border
            if (isShowTimeBgBorder) {
                canvas.drawRoundRect(mDayBgBorderRectF, mTimeBgBorderRadius, mTimeBgBorderRadius, mTimeBgBorderPaint);
            }
            if (isDrawBg) {
                // draw day background
                canvas.drawRoundRect(mDayBgRectF, mTimeBgRadius, mTimeBgRadius, mTimeBgPaint);
                if (isShowTimeBgDivisionLine) {
                    // draw day background division line
                    Point startPoint = new Point(mLeftPaddingSize + mTimeBgBorderSize, mTimeBgDivisionLineYPos);
                    Point endPoint = new Point(mLeftPaddingSize + mDayTimeBgWidth + mTimeBgBorderSize, mTimeBgDivisionLineYPos);
                    canvas.drawLine(startPoint, endPoint, mTimeBgDivisionLinePaint);
                }
            }
            // draw day text
            canvas.drawText(mTimeTextPaint, Utils.formatNum(mDay), mDayBgRectF.getHorizontalCenter(), mTimeTextBaseY);
            if (mSuffixDayTextWidth > 0) {
                // draw day suffix
                canvas.drawText(mSuffixTextPaint, mSuffixDay, mLeftPaddingSize + mDayTimeBgWidth + mSuffixDayLeftMargin + (mTimeBgBorderSize * 2), mSuffixDayTextBaseline);
            }

            // hour left point
            mHourLeft = mLeftPaddingSize + mDayTimeBgWidth + mSuffixDayTextWidth + mSuffixDayLeftMargin + mSuffixDayRightMargin + (mTimeBgBorderSize * 2);
        } else {
            // hour left point
            mHourLeft = mLeftPaddingSize;
        }

        if (isShowHour) {
            // draw hour background border
            if (isShowTimeBgBorder) {
                canvas.drawRoundRect(mHourBgBorderRectF, mTimeBgBorderRadius, mTimeBgBorderRadius, mTimeBgBorderPaint);
            }
            if (isDrawBg) {
                // draw hour background
                canvas.drawRoundRect(mHourBgRectF, mTimeBgRadius, mTimeBgRadius, mTimeBgPaint);
                if (isShowTimeBgDivisionLine) {
                    // draw hour background division line
                    canvas.drawLine(new Point(mHourLeft + mTimeBgBorderSize, mTimeBgDivisionLineYPos), new Point(mTimeBgSize + mHourLeft + mTimeBgBorderSize, mTimeBgDivisionLineYPos), mTimeBgDivisionLinePaint);
                }
            }
            // draw hour text
            canvas.drawText(mTimeTextPaint, Utils.formatNum(mHour), mHourBgRectF.getHorizontalCenter(), mTimeTextBaseY);
            if (mSuffixHourTextWidth > 0) {
                // draw hour suffix
                canvas.drawText(mSuffixTextPaint, mSuffixHour, mHourLeft + mTimeBgSize + mSuffixHourLeftMargin + (mTimeBgBorderSize * 2), mSuffixHourTextBaseline);
            }

            // minute left point
            mMinuteLeft = mHourLeft + mTimeBgSize + mSuffixHourTextWidth + mSuffixHourLeftMargin + mSuffixHourRightMargin + (mTimeBgBorderSize * 2);
        } else {
            // minute left point
            mMinuteLeft = mHourLeft;
        }

        if (isShowMinute) {
            // draw minute background border
            if (isShowTimeBgBorder) {
                canvas.drawRoundRect(mMinuteBgBorderRectF, mTimeBgBorderRadius, mTimeBgBorderRadius, mTimeBgBorderPaint);
            }
            if (isDrawBg) {
                // draw minute background
                canvas.drawRoundRect(mMinuteBgRectF, mTimeBgRadius, mTimeBgRadius, mTimeBgPaint);
                if (isShowTimeBgDivisionLine) {
                    // draw minute background division line
                    canvas.drawLine(new Point(mMinuteLeft + mTimeBgBorderSize, mTimeBgDivisionLineYPos), new Point(mTimeBgSize + mMinuteLeft + mTimeBgBorderSize, mTimeBgDivisionLineYPos), mTimeBgDivisionLinePaint);
                }
            }
            // draw minute text
            canvas.drawText(mTimeTextPaint, Utils.formatNum(mMinute), mMinuteBgRectF.getHorizontalCenter(), mTimeTextBaseY);
            if (mSuffixMinuteTextWidth > 0) {
                // draw minute suffix
                canvas.drawText(mSuffixTextPaint, mSuffixMinute, mMinuteLeft + mTimeBgSize + mSuffixMinuteLeftMargin + (mTimeBgBorderSize * 2), mSuffixMinuteTextBaseline);
            }

            // second left point
            mSecondLeft = mMinuteLeft + mTimeBgSize + mSuffixMinuteTextWidth + mSuffixMinuteLeftMargin + mSuffixMinuteRightMargin + (mTimeBgBorderSize * 2);
        } else {
            // second left point
            mSecondLeft = mMinuteLeft;
        }

        if (isShowSecond) {
            // draw second background border
            if (isShowTimeBgBorder) {
                canvas.drawRoundRect(mSecondBgBorderRectF, mTimeBgBorderRadius, mTimeBgBorderRadius, mTimeBgBorderPaint);
            }
            if (isDrawBg) {
                // draw second background
                canvas.drawRoundRect(mSecondBgRectF, mTimeBgRadius, mTimeBgRadius, mTimeBgPaint);
                if (isShowTimeBgDivisionLine) {
                    // draw second background division line
                    canvas.drawLine(new Point(mSecondLeft + mTimeBgBorderSize, mTimeBgDivisionLineYPos), new Point(mTimeBgSize + mSecondLeft + mTimeBgBorderSize, mTimeBgDivisionLineYPos), mTimeBgDivisionLinePaint);
                }
            }
            // draw second text
            canvas.drawText(mTimeTextPaint, Utils.formatNum(mSecond), mSecondBgRectF.getHorizontalCenter(), mTimeTextBaseY);
            if (mSuffixSecondTextWidth > 0) {
                // draw second suffix
                canvas.drawText(mSuffixTextPaint, mSuffixSecond, mSecondLeft + mTimeBgSize + mSuffixSecondLeftMargin + (mTimeBgBorderSize * 2), mSuffixSecondTextBaseline);
            }

            if (isShowMillisecond) {
                // draw millisecond background border
                if (isShowTimeBgBorder) {
                    canvas.drawRoundRect(mMillisecondBgBorderRectF, mTimeBgBorderRadius, mTimeBgBorderRadius, mTimeBgBorderPaint);
                }
                // millisecond left point
                float mMillisecondLeft = mSecondLeft + mTimeBgSize + mSuffixSecondTextWidth + mSuffixSecondLeftMargin + mSuffixSecondRightMargin + (mTimeBgBorderSize * 2);
                if (isDrawBg) {
                    // draw millisecond background
                    canvas.drawRoundRect(mMillisecondBgRectF, mTimeBgRadius, mTimeBgRadius, mTimeBgPaint);
                    if (isShowTimeBgDivisionLine) {
                        // draw millisecond background division line
                        canvas.drawLine(new Point(mMillisecondLeft + mTimeBgBorderSize, mTimeBgDivisionLineYPos), new Point(mTimeBgSize + mMillisecondLeft + mTimeBgBorderSize, mTimeBgDivisionLineYPos), mTimeBgDivisionLinePaint);
                    }
                }
                // draw millisecond text
                canvas.drawText(mTimeTextPaint, Utils.formatMillisecond(mMillisecond), mMillisecondBgRectF.getHorizontalCenter(), mTimeTextBaseY);
                if (mSuffixMillisecondTextWidth > 0) {
                    // draw millisecond suffix
                    canvas.drawText(mSuffixTextPaint, mSuffixMillisecond, mMillisecondLeft + mTimeBgSize + mSuffixMillisecondLeftMargin + (mTimeBgBorderSize * 2), mSuffixMillisecondTextBaseline);
                }
            }
        }
    }

    public void setTimeBgSize(float size) {
        mTimeBgSize = Utils.vp2px(mContext, size);
    }


    public void setTimeBgColor(int color) {
        mTimeBgColor = color;
        mTimeBgPaint.setColor(new Color(mTimeBgColor));
        if (color == Color.TRANSPARENT.getValue() && isShowTimeBgBorder) {
            isDrawBg = false;
            mTimeBgBorderPaint.setStrokeWidth(mTimeBgBorderSize);
            mTimeBgBorderPaint.setStyle(Paint.Style.STROKE_STYLE);
        } else {
            isDrawBg = true;
            if (isShowTimeBgBorder) {
                mTimeBgBorderPaint.setStrokeWidth(0);
                mTimeBgBorderPaint.setStyle(Paint.Style.FILL_STYLE);
            }
        }
    }

    public void setTimeBgRadius(float radius) {
        mTimeBgRadius = Utils.vp2px(mContext, radius);
    }

    public float getTimeBgRadius() {
        return mTimeBgRadius;
    }

    public void setIsShowTimeBgDivisionLine(boolean isShow) {
        isShowTimeBgDivisionLine = isShow;
        if (isShowTimeBgDivisionLine) {
            initTimeTextBgDivisionLinePaint();
        } else {
            mTimeBgDivisionLinePaint = null;
        }
    }

    public boolean getIsShowTimeBgDivisionLine() {
        return isShowTimeBgDivisionLine;
    }

    public void setTimeBgDivisionLineColor(int color) {
        mTimeBgDivisionLineColor = color;
        if (null != mTimeBgDivisionLinePaint) {
            mTimeBgDivisionLinePaint.setColor(new Color(mTimeBgDivisionLineColor));
        }
    }

    public int getTimeBgDivisionLineColor() {
        return mTimeBgDivisionLineColor;
    }

    public void setTimeBgDivisionLineSize(float size) {
        mTimeBgDivisionLineSize = Utils.vp2px(mContext, size);
        if (null != mTimeBgDivisionLinePaint) {
            mTimeBgDivisionLinePaint.setStrokeWidth(mTimeBgDivisionLineSize);
        }
    }

    public void setIsShowTimeBgBorder(boolean isShow) {
        isShowTimeBgBorder = isShow;
        if (isShowTimeBgBorder) {
            initTimeBgBorderPaint();
        } else {
            mTimeBgBorderPaint = null;
            mTimeBgBorderSize = 0;
        }
    }

    public boolean getIsShowTimeBgBorder() {
        return isShowTimeBgBorder;
    }

    public void setTimeBgBorderColor(int color) {
        mTimeBgBorderColor = color;
        if (null != mTimeBgBorderPaint) {
            mTimeBgBorderPaint.setColor(new Color(mTimeBgBorderColor));
        }
    }

    public int getmTimeBgBorderColor() {
        return mTimeBgBorderColor;
    }

    public void setTimeBgBorderSize(float size) {
        mTimeBgBorderSize = Utils.vp2px(mContext, size);
        if (null != mTimeBgBorderPaint && !isDrawBg) {
            mTimeBgBorderPaint.setStrokeWidth(mTimeBgBorderSize);
            mTimeBgBorderPaint.setStyle(Paint.Style.STROKE_STYLE);
        }
    }

    public float getmTimeBgBorderSize() {
        return mTimeBgBorderSize;
    }

    public void setTimeBgBorderRadius(float size) {
        mTimeBgBorderRadius = Utils.vp2px(mContext, size);
    }

    public float getmTimeBgBorderRadius() {
        return mTimeBgBorderRadius;
    }

}
