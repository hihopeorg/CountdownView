﻿# CountdownView

**本项目是基于开源项目CountdownView进行ohos化的移植和开发的，可以通过项目标签以及github地址（ https://github.com/iwgang/CountdownView ）追踪到原项目版本**

#### 项目介绍

- 项目名称：倒计时控件
- 所属系列：ohos的第三方组件适配移植
- 功能：倒计时控件，使用Canvas绘制，支持多种样式
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/iwgang/CountdownView
- 原项目基线版本：v2.1.6
- 编程语言：Java
- 外部库依赖：无

#### 演示效果
![](animation.gif)

#### 安装教程
##### 方案一：
1. 编译依赖库 library-release.har。
2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```
4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。
##### 方案二：
  1. 在工程的build.gradle的allprojects中，添加har所在的Maven仓地址:
```
 repositories {
     maven {
         url 'http://106.15.92.248:8081/repository/Releases/' 
     }
 }
```
  2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
 dependencies {
     implementation 'com.github.iwgang:countdownview:1.0.2'
 }
```

#### 使用说明

 1、初始化缓存库
```
CountdownView mCvCountdownView = (CountdownView)findComponentById(ResourceTable.Id_cv_countdownViewTest1);
mCvCountdownView.start(995550000); // 毫秒

// 或者自己编写倒计时逻辑，然后调用updateShow来更新UI
for (int time=0; time<1000; time++) {
    mCvCountdownView.updateShow(time);
}
```

### Layout
``` xml
<cn.iwgang.countdownview.CountdownView
    app:isHideTimeBackground="false"
    app:isShowDay="false"
    app:isShowHour="true"
    app:isShowMillisecond="false"
    app:isShowMinute="true"
    app:isShowSecond="true"
    app:isShowTimeBgDivisionLine="false"
    app:suffixGravity="center"
    app:suffixTextColor="#000000"
    app:suffixTextSize="20fp"
    app:timeBgColor="#FF5000"
    app:timeTextColor="#FFFFFF"
    app:timeTextSize="20fp"
    ohos:height="50vp"
    ohos:width="match_content"/>
```

### 定制
|    参数    |   类型   |  默认值 |
| --------   | :-----:  | :----:  |
|isHideTimeBackground | boolean | true|
|timeBgColor  | color      | #444444|
|timeBgSize   | dimension  | timeSize + 2vp * 4|
|timeBgRadius | dimension  | 0|
|isShowTimeBgDivisionLine | boolean  | true|
|timeBgDivisionLineColor | color | #30FFFFFF|
|timeBgDivisionLineSize  | dimension | 0.5vp|
|timeTextSize   | dimension | 12fp | 
|timeTextColor  | color | #000000|
|isTimeTextBold | boolean | false|
|isShowDay  | boolean | 自动显示 (天 > 1 显示, = 0 隐藏)|
|isShowHour  | boolean | 自动显示 (小时 > 1 显示， = 0 隐藏)|
|isShowMinute  | boolean | true|
|isShowSecond  | boolean | true|
|isShowMillisecond  | boolean | false|
|isConvertDaysToHours | boolean | false|
|suffixTextSize | dimension | 12fp|
|suffixTextColor  | color | #000000|
|isSuffixTextBold  | boolean | false|
|suffixGravity | 'top' or 'center' or 'bottom' | 'center'|
|suffix | string | ':'|
|suffixDay  | string | null|
|suffixHour  | string | null|
|suffixMinute  | string | null|
|suffixSecond  | string | null|
|suffixMillisecond  | string | null|
|suffixLRMargin  | dimension | left 3vp right 3vp|
|suffixDayLeftMargin | dimension | 0|
|suffixDayRightMargin  | dimension | 0|
|suffixHourLeftMargin  | dimension | 0|
|suffixHourRightMargin  | dimension | 0|
|suffixMinuteLeftMargin | dimension | 0|
|suffixMinuteRightMargin  | dimension | 0|
|suffixSecondLeftMargin  | dimension | 0|
|suffixSecondRightMargin  | dimension | 0|
|suffixMillisecondLeftMargin | dimension | 0|
|isShowTimeBgBorder | boolean | false|
|timeBgBorderColor  | color | #000000|
|timeBgBorderSize  | dimension | 1vp|
|timeBgBorderRadius  | dimension | 0|

### 其它
* **多个CountdownView时，给每个指定值**
```
// 第1步，设置tag
mCvCountdownView.setTag(R.id.name, uid);
// 第2步，从回调中的CountdownView取回tag
@Override
public void onEnd(CountdownView cv) {
    Object nameTag = cv.getTag(R.id.uid);
    if (null != nameTag) {
        Hilog.info(label, "name = " + nameTag.toString());
    }
}
```

* **动态显示设置, 支持所有xml中的配置项来使用java代码设置**
```
dynamicShow(DynamicConfig dynamicConfig)
```
* **倒计时结束后回调**
```
setOnCountdownEndListener(OnCountdownEndListener onCountdownEndListener);
```
* **指定间隔时间回调**
```
setOnCountdownIntervalListener(long interval, OnCountdownIntervalListener onCountdownIntervalListener);
```

#### 版本迭代

- v1.0.2

#### 版权和许可信息
- MIT License
