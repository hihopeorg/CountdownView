/*
 * The MIT License (MIT)
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package cn.iwgang.countdownviewdemo;

import cn.iwgang.countdownview.Utils;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public class ColorPicker extends CommonDialog {

    private OnColorPickListener listener;

    public ColorPicker(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        int windowHeight = AttrHelper.vp2px(context.getResourceManager().getDeviceCapability().height, context);
        int windowWidth = AttrHelper.vp2px(context.getResourceManager().getDeviceCapability().width, context);
        Component parse = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout_color_picker, null, false);

        this.setSize((int) (windowWidth * 0.8), (int) (windowHeight * 0.33));
        this.setContentCustomComponent(parse);

        parse.findComponentById(ResourceTable.Id_btnCancel).setClickedListener(component -> {
            this.hide();
        });

        ListContainer listContainer = (ListContainer) parse.findComponentById(ResourceTable.Id_lcList);
        List<int[]> list = new ArrayList<>();
        list.add(new int[]{220, 20, 60});
        list.add(new int[]{218, 112, 214});
        list.add(new int[]{139, 0, 139});
        list.add(new int[]{138, 43, 226});
        list.add(new int[]{0, 0, 205});
        list.add(new int[]{30, 144, 255});
        list.add(new int[]{0, 191, 255});
        list.add(new int[]{0, 255, 255});
        list.add(new int[]{245, 255, 250});
        list.add(new int[]{0, 255, 0});
        list.add(new int[]{173, 255, 47});
        list.add(new int[]{255, 255, 0});
        list.add(new int[]{255, 215, 0});
        list.add(new int[]{255, 222, 173});
        list.add(new int[]{255, 160, 122});

        listContainer.setItemProvider(new MyItemPrivider(context, list));
        TableLayoutManager tableLayoutManager = new TableLayoutManager();
        tableLayoutManager.setColumnCount(5);
        listContainer.setLayoutManager(tableLayoutManager);
        listContainer.setItemClickedListener((listContainer1, component, i, l) -> {
            int[] color = list.get(i);
            if (listener != null) {
                listener.onColorPicked(Color.rgb(color[0], color[1], color[2]));
            }
            this.hide();
        });
    }

    public interface OnColorPickListener {
        void onColorPicked(int pickedColor);
    }

    public void setOnColorPickListener(OnColorPickListener listener) {
        this.listener = listener;
    }

    @Override
    public void show() {
        super.show();
    }

    private class MyItemPrivider extends BaseItemProvider {

        private List<int[]> colors;
        private Context context;

        public MyItemPrivider(Context context, List<int[]> colors) {
            this.context = context;
            this.colors = colors;
        }

        @Override
        public int getCount() {
            return colors.size();
        }

        @Override
        public int[] getItem(int i) {
            return colors.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
            int[] color = colors.get(i);

            component = new Component(context);
            component.setWidth(Utils.vp2px(context, 50));
            component.setHeight(Utils.vp2px(context, 50));

            ShapeElement element = new ShapeElement();
            element.setRgbColor(new RgbColor(color[0], color[1], color[2]));
            component.setBackground(element);

            return component;
        }
    }
}
