package cn.iwgang.countdownviewdemo;


import cn.iwgang.countdownview.CountdownView;
import cn.iwgang.countdownview.DynamicConfig;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;


@SuppressWarnings("ALL")
public class DynamicShowAbility extends Ability {
    private final long TIME = (long) 8 * 24 * 60 * 60 * 1000;


    private CountdownView mCvCountdownViewTest, mCvCountdownViewTestHasBg;
    private Component mLlBackgroundConfigContainer;
    private Component mLlConvertDaysToHoursContainer;

    private boolean isConvertDaysToHours = false;
    private boolean hasBackgroundCountdownView = false;
    private float timeTextSize = 22;
    private float suffixTextSize = 18;
    private boolean isShowDay = true, isShowHour = true, isShowMinute = true, isShowSecond = true, isShowMillisecond = true;
    private float timeBgSize = 40;
    private float bgRadius = 3;
    private float bgBorderSize;
    private float bgBorderRadius;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_dynamic);

        mCvCountdownViewTest = (CountdownView) findComponentById(ResourceTable.Id_cv_countdownViewTest);
        mCvCountdownViewTestHasBg = (CountdownView) findComponentById(ResourceTable.Id_cv_countdownViewTestHasBg);
        mLlBackgroundConfigContainer = findComponentById(ResourceTable.Id_ll_backgroundConfigContainer);
        mLlConvertDaysToHoursContainer = findComponentById(ResourceTable.Id_ll_convertDaysToHoursContainer);

        mCvCountdownViewTest.start(TIME);

        findComponentById(ResourceTable.Id_btn_theme1).setClickedListener(component -> {
            DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
            if (hasBackgroundCountdownView) {
                DynamicConfig.BackgroundInfo backgroundInfo = new DynamicConfig.BackgroundInfo();
                backgroundInfo.setColor(0xFFFF54BC)
                        .setSize(30f)
                        .setRadius(0f)
                        .setShowTimeBgDivisionLine(false);
                dynamicConfigBuilder.setTimeTextSize(15)
                        .setTimeTextColor(0xFFFFFFFF)
                        .setTimeTextBold(true)
                        .setSuffixTextColor(0xFF000000)
                        .setSuffixTextSize(15)
                        .setBackgroundInfo(backgroundInfo)
                        .setShowDay(false).setShowHour(true).setShowMinute(true).setShowSecond(true).setShowMillisecond(true);
            } else {
                dynamicConfigBuilder.setTimeTextSize(35)
                        .setTimeTextColor(0xFFFF5000)
                        .setTimeTextBold(true)
                        .setSuffixTextColor(0xFFFF5000)
                        .setSuffixTextSize(30)
                        .setSuffixTextBold(false)
                        .setSuffix(":")
                        .setSuffixMillisecond("") // Remove millisecond suffix
                        .setSuffixGravity(DynamicConfig.SuffixGravity.CENTER)
                        .setShowDay(false).setShowHour(false).setShowMinute(true).setShowSecond(true).setShowMillisecond(true);
            }
            if (hasBackgroundCountdownView) {
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            } else {
                mCvCountdownViewTest.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        findComponentById(ResourceTable.Id_btn_theme2).setClickedListener(component -> {
            DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
            if (hasBackgroundCountdownView) {
                DynamicConfig.BackgroundInfo backgroundInfo = new DynamicConfig.BackgroundInfo();
                backgroundInfo.setColor(0xFFFF5000)
                        .setSize(60f)
                        .setRadius(30f)
                        .setShowTimeBgDivisionLine(false);
                dynamicConfigBuilder.setTimeTextSize(42)
                        .setTimeTextColor(0xFFFFFFFF)
                        .setTimeTextBold(true)
                        .setSuffixTextColor(0xFF000000)
                        .setSuffixTextSize(42)
                        .setSuffixTextBold(true)
                        .setBackgroundInfo(backgroundInfo)
                        .setShowDay(false).setShowHour(true).setShowMinute(true).setShowSecond(true).setShowMillisecond(false);
            } else {
                dynamicConfigBuilder.setTimeTextSize(60)
                        .setTimeTextColor(0xFF444444)
                        .setTimeTextBold(false)
                        .setSuffixTextColor(0xFF444444)
                        .setSuffixTextSize(20)
                        .setSuffixTextBold(false)
                        .setSuffixMinute("m")
                        .setSuffixMinuteLeftMargin(5)
                        .setSuffixMinuteRightMargin(10)
                        .setSuffixSecond("s")
                        .setSuffixSecondLeftMargin(5)
                        .setSuffixGravity(DynamicConfig.SuffixGravity.BOTTOM)
                        .setShowDay(false).setShowHour(false).setShowMinute(true).setShowSecond(true).setShowMillisecond(false);
            }
            if (hasBackgroundCountdownView) {
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            } else {
                mCvCountdownViewTest.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        findComponentById(ResourceTable.Id_btn_theme3).setClickedListener(component -> {
            DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
            if (hasBackgroundCountdownView) {
                DynamicConfig.BackgroundInfo backgroundInfo = new DynamicConfig.BackgroundInfo();
                backgroundInfo.setColor(0xFF444444)
                        .setSize(40f)
                        .setRadius(3f)
                        .setShowTimeBgDivisionLine(true)
                        .setDivisionLineColor(Color.getIntColor("#30FFFFFF"))
                        .setDivisionLineSize(1f);
                dynamicConfigBuilder.setTimeTextSize(22)
                        .setTimeTextColor(0xFFFFFFFF)
                        .setTimeTextBold(true)
                        .setSuffixTextColor(0xFF000000)
                        .setSuffixTextSize(18)
                        .setSuffixTextBold(true)
                        .setBackgroundInfo(backgroundInfo)
                        .setShowDay(true).setShowHour(true).setShowMinute(true).setShowSecond(true).setShowMillisecond(true);
            } else {
                dynamicConfigBuilder.setTimeTextSize(22)
                        .setTimeTextColor(0xFF000000)
                        .setTimeTextBold(false)
                        .setSuffixTextColor(0xFF000000)
                        .setSuffixTextSize(12)
                        .setSuffixTextBold(false)
                        .setSuffixDay("天").setSuffixHour("小时").setSuffixMinute("分钟").setSuffixSecond("秒").setSuffixMillisecond("毫秒")
                        .setSuffixGravity(DynamicConfig.SuffixGravity.TOP)
                        .setShowDay(true).setShowHour(true).setShowMinute(true).setShowSecond(true).setShowMillisecond(true);
            }
            if (hasBackgroundCountdownView) {
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            } else {
                mCvCountdownViewTest.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        findComponentById(ResourceTable.Id_btn_timeTextSizePlus).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setTimeTextSize(++timeTextSize);
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfigBuilder.build());
                }
            }
        });

        findComponentById(ResourceTable.Id_btn_timeTextSizeSubtract).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                if (timeTextSize == 0) return;
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setTimeTextSize(--timeTextSize);
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfigBuilder.build());
                }
            }
        });

        findComponentById(ResourceTable.Id_btn_modTimeTextColor).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                ColorPicker picker = new ColorPicker(DynamicShowAbility.this);
                picker.setOnColorPickListener(new ColorPicker.OnColorPickListener() {
                    @Override
                    public void onColorPicked(int pickedColor) {
                        DynamicConfig dynamicConfig = new DynamicConfig.Builder().setTimeTextColor(pickedColor).build();
                        if (hasBackgroundCountdownView) {
                            mCvCountdownViewTestHasBg.dynamicShow(dynamicConfig);
                        } else {
                            mCvCountdownViewTest.dynamicShow(dynamicConfig);
                        }
                    }
                });
                picker.show();
            }
        });

        findComponentById(ResourceTable.Id_btn_suffixTextSizePlus).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setSuffixTextSize(++suffixTextSize);
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfigBuilder.build());
                }
            }
        });

        findComponentById(ResourceTable.Id_btn_suffixTextSizeSubtract).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                if (timeTextSize == 0) return;
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setSuffixTextSize(--suffixTextSize);
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfigBuilder.build());
                }
            }
        });

        findComponentById(ResourceTable.Id_btn_modSuffixTextColor).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                ColorPicker picker = new ColorPicker(DynamicShowAbility.this);
                picker.setOnColorPickListener(new ColorPicker.OnColorPickListener() {
                    @Override
                    public void onColorPicked(int pickedColor) {
                        DynamicConfig dynamicConfig = new DynamicConfig.Builder().setSuffixTextColor(pickedColor).build();
                        if (hasBackgroundCountdownView) {
                            mCvCountdownViewTestHasBg.dynamicShow(dynamicConfig);
                        } else {
                            mCvCountdownViewTest.dynamicShow(dynamicConfig);
                        }
                    }
                });
                picker.show();
            }
        });

        findComponentById(ResourceTable.Id_btn_suffixGravityTop).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                DynamicConfig dynamicConfig = new DynamicConfig.Builder().setSuffixGravity(DynamicConfig.SuffixGravity.TOP).build();
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfig);
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfig);
                }
            }
        });

        findComponentById(ResourceTable.Id_btn_suffixGravityCenter).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                DynamicConfig dynamicConfig = new DynamicConfig.Builder().setSuffixGravity(DynamicConfig.SuffixGravity.CENTER).build();
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfig);
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfig);
                }
            }
        });

        findComponentById(ResourceTable.Id_btn_suffixGravityBottom).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                DynamicConfig dynamicConfig = new DynamicConfig.Builder().setSuffixGravity(DynamicConfig.SuffixGravity.BOTTOM).build();
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfig);
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfig);
                }
            }
        });

        findComponentById(ResourceTable.Id_btn_refTimeShow).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                if (!isShowDay && !isShowHour && !isShowMinute && !isShowSecond && !isShowMillisecond) {
                    new ToastDialog(getContext()).setText("Select at least one item").show();
                    return;
                }
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setShowDay(isShowDay)
                        .setShowHour(isShowHour)
                        .setShowMinute(isShowMinute)
                        .setShowSecond(isShowSecond)
                        .setShowMillisecond(isShowMillisecond);

                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfigBuilder.build());
                }
            }
        });

        findComponentById(ResourceTable.Id_btn_bgSizePlus).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setSize(++timeBgSize));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        findComponentById(ResourceTable.Id_btn_bgSizeSubtract).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                if (timeBgSize == 0) return;
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setSize(--timeBgSize));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        findComponentById(ResourceTable.Id_btn_modBgColor).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                ColorPicker picker = new ColorPicker(DynamicShowAbility.this);
                picker.setOnColorPickListener(new ColorPicker.OnColorPickListener() {
                    @Override
                    public void onColorPicked(int pickedColor) {
                        DynamicConfig dynamicConfig = new DynamicConfig.Builder().setBackgroundInfo(new DynamicConfig.BackgroundInfo().setColor(pickedColor)).build();
                        mCvCountdownViewTestHasBg.dynamicShow(dynamicConfig);
                    }
                });
                picker.show();
            }
        });

        findComponentById(ResourceTable.Id_btn_bgRadiusPlus).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setRadius(++bgRadius));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        findComponentById(ResourceTable.Id_btn_bgRadiusSubtract).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                if (bgRadius == 0) return;
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setRadius(--bgRadius));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        final Button btnModBgDivisionLineColor = (Button) findComponentById(ResourceTable.Id_btn_modBgDivisionLineColor);
        btnModBgDivisionLineColor.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                ColorPicker picker = new ColorPicker(DynamicShowAbility.this);
                picker.setOnColorPickListener(new ColorPicker.OnColorPickListener() {
                    @Override
                    public void onColorPicked(int pickedColor) {
                        DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                        dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setShowTimeBgDivisionLine(true).setDivisionLineColor(pickedColor));
                        mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
                    }
                });
                picker.show();
            }
        });

        ((Checkbox) findComponentById(ResourceTable.Id_cb_bgDivisionLine)).setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                btnModBgDivisionLineColor.setEnabled(isChecked);

                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setShowTimeBgDivisionLine(isChecked));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        final Button btnBgBorderSizePlus = (Button) findComponentById(ResourceTable.Id_btn_bgBorderSizePlus);
        final Button btnBgBorderSizeSubtract = (Button) findComponentById(ResourceTable.Id_btn_bgBorderSizeSubtract);
        final Button btnBgBorderRadiusPlus = (Button) findComponentById(ResourceTable.Id_btn_bgBorderRadiusPlus);
        final Button btnBgBorderRadiusSubtract = (Button) findComponentById(ResourceTable.Id_btn_bgBorderRadiusSubtract);

        btnBgBorderSizePlus.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setShowTimeBgBorder(true).setBorderSize(++bgBorderSize));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        btnBgBorderSizeSubtract.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                if (bgBorderSize == 0) return;
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setShowTimeBgBorder(true).setBorderSize(--bgBorderSize));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        btnBgBorderRadiusPlus.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setShowTimeBgBorder(true).setBorderRadius(++bgBorderRadius));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        btnBgBorderRadiusSubtract.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                if (bgBorderRadius == 0) return;
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setShowTimeBgBorder(true).setBorderRadius(--bgBorderRadius));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        final Button btnModBgBorderColor = (Button) findComponentById(ResourceTable.Id_btn_modBgBorderColor);
        btnModBgBorderColor.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                ColorPicker picker = new ColorPicker(DynamicShowAbility.this);
                picker.setOnColorPickListener(new ColorPicker.OnColorPickListener() {
                    @Override
                    public void onColorPicked(int pickedColor) {
                        DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                        dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setShowTimeBgBorder(true).setBorderColor(pickedColor));
                        mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
                    }
                });
                picker.show();
            }
        });

        ((Checkbox) findComponentById(ResourceTable.Id_cb_bgBorder)).setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                btnModBgBorderColor.setEnabled(isChecked);
                btnBgBorderSizePlus.setEnabled(isChecked);
                btnBgBorderSizeSubtract.setEnabled(isChecked);
                btnBgBorderRadiusPlus.setEnabled(isChecked);
                btnBgBorderRadiusSubtract.setEnabled(isChecked);

                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setShowTimeBgBorder(isChecked));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        ((Checkbox) findComponentById(ResourceTable.Id_cb_bgBorder)).setChecked(false);

        final TextField etSuffixDay = (TextField) findComponentById(ResourceTable.Id_et_suffixDay);
        final TextField etSuffixHour = (TextField) findComponentById(ResourceTable.Id_et_suffixHour);
        final TextField etSuffixMinute = (TextField) findComponentById(ResourceTable.Id_et_suffixMinute);
        final TextField etSuffixSecond = (TextField) findComponentById(ResourceTable.Id_et_suffixSecond);
        final TextField etSuffixMillisecond = (TextField) findComponentById(ResourceTable.Id_et_suffixMillisecond);
        findComponentById(ResourceTable.Id_btm_refSuffix).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setSuffixDay(etSuffixDay.getText().toString())
                        .setSuffixHour(etSuffixHour.getText().toString())
                        .setSuffixMinute(etSuffixMinute.getText().toString())
                        .setSuffixSecond(etSuffixSecond.getText().toString())
                        .setSuffixMillisecond(etSuffixMillisecond.getText().toString());
                mCvCountdownViewTest.dynamicShow(dynamicConfigBuilder.build());
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        ((Checkbox) findComponentById(ResourceTable.Id_cb_hasBackground)).setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                mCvCountdownViewTestHasBg.setHeight(AttrHelper.vp2px(80, getContext()));
                hasBackgroundCountdownView = isChecked;
                mLlBackgroundConfigContainer.setVisibility(isChecked ? Component.VISIBLE : Component.HIDE);
                mCvCountdownViewTest.setVisibility(!isChecked ? Component.VISIBLE : Component.HIDE);
                mCvCountdownViewTestHasBg.setVisibility(isChecked ? Component.VISIBLE : Component.HIDE);

                if (isChecked) {
                    mCvCountdownViewTest.stop();
                    mCvCountdownViewTestHasBg.start(TIME);
                    mLlConvertDaysToHoursContainer.setVisibility(Component.HIDE);
                } else {
                    mCvCountdownViewTestHasBg.stop();
                    mCvCountdownViewTest.start(TIME);
                    mLlConvertDaysToHoursContainer.setVisibility(Component.VISIBLE);
                }
            }
        });

        final Button btnConvertDaysToHours = (Button) findComponentById(ResourceTable.Id_btn_convertDaysToHours);
        findComponentById(ResourceTable.Id_btn_convertDaysToHours).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                isConvertDaysToHours = !isConvertDaysToHours;
                btnConvertDaysToHours.setText(isConvertDaysToHours ? "转换" : "不转换");

                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setConvertDaysToHours(isConvertDaysToHours);
                dynamicConfigBuilder.setShowDay(!isConvertDaysToHours);
                mCvCountdownViewTest.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        handlerCheckBoxSel();
    }

    private void handlerCheckBoxSel() {
        final Checkbox cbDay = (Checkbox) findComponentById(ResourceTable.Id_cb_day);
        final Checkbox cbHour = (Checkbox) findComponentById(ResourceTable.Id_cb_hour);
        final Checkbox cbMinute = (Checkbox) findComponentById(ResourceTable.Id_cb_minute);
        final Checkbox cbSecond = (Checkbox) findComponentById(ResourceTable.Id_cb_second);
        final Checkbox cbMillisecond = (Checkbox) findComponentById(ResourceTable.Id_cb_millisecond);

        cbDay.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                DynamicShowAbility.this.isShowDay = isChecked;
            }
        });
        cbHour.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                DynamicShowAbility.this.isShowHour = isChecked;
            }
        });
        cbMinute.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                DynamicShowAbility.this.isShowMinute = isChecked;
            }
        });
        cbSecond.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                DynamicShowAbility.this.isShowSecond = isChecked;
                if (!isChecked && DynamicShowAbility.this.isShowMillisecond) {
                    cbMillisecond.setChecked(false);
                }
            }
        });
        cbMillisecond.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                DynamicShowAbility.this.isShowMillisecond = isChecked;
                if (isChecked && !DynamicShowAbility.this.isShowSecond) {
                    cbSecond.setChecked(true);
                }
            }
        });
    }

}


