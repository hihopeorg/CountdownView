package cn.iwgang.countdownviewdemo;

import cn.iwgang.countdownview.CountdownView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class MainAbility extends Ability implements CountdownView.OnCountdownEndListener {

    private HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 11, "-->> ");
    private long time = 0;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);

        CountdownView mCvCountdownViewTest1 = (CountdownView) findComponentById(ResourceTable.Id_cv_countdownViewTest1);
        mCvCountdownViewTest1.setTag("test1");

        long time1 = (long) 5 * 60 * 60 * 1000;
        mCvCountdownViewTest1.start(time1);

        CountdownView mCvCountdownViewTest2 = (CountdownView) findComponentById(ResourceTable.Id_cv_countdownViewTest2);
        mCvCountdownViewTest2.setTag("test2");
        long time2 = (long) 30 * 60 * 1000;
        mCvCountdownViewTest2.start(time2);

        CountdownView cvCountdownViewTest211 = (CountdownView) findComponentById(ResourceTable.Id_cv_countdownViewTest211);
        cvCountdownViewTest211.setTag("test21");
        long time211 = (long) 30 * 60 * 1000;
        cvCountdownViewTest211.start(time211);

        CountdownView mCvCountdownViewTest21 = (CountdownView) findComponentById(ResourceTable.Id_cv_countdownViewTest21);
        mCvCountdownViewTest21.setTag("test21");
        long time21 = (long) 24 * 60 * 60 * 1000;
        mCvCountdownViewTest21.start(time21);

        CountdownView mCvCountdownViewTest22 = (CountdownView) findComponentById(ResourceTable.Id_cv_countdownViewTest22);
        mCvCountdownViewTest22.setTag("test22");
        long time22 = (long) 30 * 60 * 1000;
        mCvCountdownViewTest22.start(time22);

        CountdownView mCvCountdownViewTest3 = (CountdownView) findComponentById(ResourceTable.Id_cv_countdownViewTest3);
        long time3 = (long) 9 * 60 * 60 * 1000;
        mCvCountdownViewTest3.start(time3);

        CountdownView mCvCountdownViewTest4 = (CountdownView) findComponentById(ResourceTable.Id_cv_countdownViewTest4);
        long time4 = (long) 150 * 24 * 60 * 60 * 1000;
        mCvCountdownViewTest4.start(time4);

        CountdownView cv_convertDaysToHours = (CountdownView) findComponentById(ResourceTable.Id_cv_convertDaysToHours);
        // long timeConvertDaysToHours = (long) 150 * 24 * 60 * 60 * 1000;
        cv_convertDaysToHours.start(time4);

        final CountdownView mCvCountdownViewTest5 = (CountdownView) findComponentById(ResourceTable.Id_cv_countdownViewTest5);

        EventHandler handler = new EventHandler(EventRunner.create()) {
            @Override
            protected void processEvent(InnerEvent event) {
                super.processEvent(event);
                time += 1000;
                new EventHandler(EventRunner.getMainEventRunner()).postTask(() -> {
                    mCvCountdownViewTest5.updateShow(time);
                });
                this.sendEvent(0, 1000);
            }
        };
        handler.sendEvent(0, 1000);

        CountdownView mCvCountdownViewTest6 = (CountdownView) findComponentById(ResourceTable.Id_cv_countdownViewTest6);
        long time6 = (long) 2 * 60 * 60 * 1000;
        mCvCountdownViewTest6.start(time6);


        findComponentById(ResourceTable.Id_btn_toDynamicShowAbility).setClickedListener(component -> {
            Intent in = new Intent();
            in.setOperation(new Intent.OperationBuilder()
                    .withAbilityName(DynamicShowAbility.class.getName())
                    .withBundleName(getBundleName()).build()
            );
            startAbility(in);
        });

        findComponentById(ResourceTable.Id_btn_toListViewAbility).setClickedListener(component -> {
            Intent in = new Intent();
            in.setOperation(new Intent.OperationBuilder()
                    .withAbilityName(ListViewAbility.class.getName())
                    .withBundleName(getBundleName()).build()
            );
            startAbility(in);
        });

    }

    @Override
    public void onEnd(CountdownView cv) {
        Object tag = cv.getTag();
        if (null != tag) {
            HiLog.info(label, "tag = " + tag.toString());
        }
    }
}