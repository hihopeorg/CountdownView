package cn.iwgang.countdownviewdemo;


import cn.iwgang.countdownview.CountdownView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;


/**
     此类模拟在ListView中使用倒计时,
     复用 本地的计时器 —— System.currentTimeMillis(), 不必自行计时
 */
public class ListViewAbility extends Ability {
    private List<ItemInfo> mDataList;
    private MyListAdapter mMyAdapter;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_listview);

        initData();

        ListContainer lvList = (ListContainer) findComponentById(ResourceTable.Id_lv_list);
        lvList.setItemProvider(mMyAdapter = new MyListAdapter(this, mDataList));
    }

    private void initData() {
        mDataList = new ArrayList<>();

        for (int i = 1; i < 20; i++) {
            mDataList.add(new ItemInfo(1000 + i, "ListContainer_测试标题_" + i, i * 20 * 1000));
        }

        // 校对倒计时
        long curTime = System.currentTimeMillis();
        for (ItemInfo itemInfo : mDataList) {
            itemInfo.setEndTime(curTime + itemInfo.getCountdown());
        }
    }

    static class MyListAdapter extends BaseItemProvider {
        private Context mContext;
        private List<ItemInfo> mDatas;

        public MyListAdapter(Context context, List<ItemInfo> datas) {
            this.mContext = context;
            this.mDatas = datas;
        }

        @Override
        public int getCount() {
            return mDatas.size();
        }

        @Override
        public Object getItem(int position) {
            return mDatas.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Component getComponent(int position, Component convertView, ComponentContainer componentContainer) {
            final MyViewHolder holder;
            if (convertView == null) {
                convertView = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_list_item, componentContainer, false);
                holder = new MyViewHolder();
                holder.initView(convertView);
                convertView.setTag(holder);
            } else {
                holder = (MyViewHolder) convertView.getTag();
            }

            ItemInfo curItemInfo = mDatas.get(position);
            holder.bindData(curItemInfo);

            dealWithLifeCycle(holder, position);

            return convertView;
        }

        private void dealWithLifeCycle(final MyViewHolder holder, final int position) {
            holder.getCvCountdownView().setBindStateChangedListener(new Component.BindStateChangedListener() {

                @Override
                public void onComponentBoundToWindow(Component component) {
                    int pos = position;
                    ItemInfo itemInfo = mDatas.get(pos);
                    holder.refreshTime(itemInfo.getEndTime() - System.currentTimeMillis());
                }

                @Override
                public void onComponentUnboundFromWindow(Component component) {
                    holder.getCvCountdownView().stop();
                }
            });
        }

        static class MyViewHolder {
            private Text mTvTitle;
            private CountdownView mCvCountdownView;
            private ItemInfo mItemInfo;

            public void initView(Component convertView) {
                mTvTitle = (Text) convertView.findComponentById(ResourceTable.Id_tv_title);
                mCvCountdownView = (CountdownView) convertView.findComponentById(ResourceTable.Id_cv_countdownView);
            }

            public void bindData(ItemInfo itemInfo) {
                mItemInfo = itemInfo;
                mTvTitle.setText(itemInfo.getTitle());
                refreshTime(mItemInfo.getEndTime() - System.currentTimeMillis());
            }

            public void refreshTime(long leftTime) {
                if (leftTime > 0) {
                    mCvCountdownView.start(leftTime);
                } else {
                    mCvCountdownView.stop();
                    mCvCountdownView.allShowZero();
                }
            }

            public ItemInfo getBean() {
                return mItemInfo;
            }

            public CountdownView getCvCountdownView() {
                return mCvCountdownView;
            }
        }
    }

    static class ItemInfo {
        private int id;
        private String title;
        private long countdown;
        /*
           根据服务器返回的countdown换算成手机对应的开奖时间 (毫秒)
           [正常情况最好由服务器返回countdown字段，然后客户端再校对成该手机对应的时间，不然误差很大]
         */
        private long endTime;

        public ItemInfo(int id, String title, long countdown) {
            this.id = id;
            this.title = title;
            this.countdown = countdown;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public long getCountdown() {
            return countdown;
        }

        public void setCountdown(long countdown) {
            this.countdown = countdown;
        }

        public long getEndTime() {
            return endTime;
        }

        public void setEndTime(long endTime) {
            this.endTime = endTime;
        }
    }

}


