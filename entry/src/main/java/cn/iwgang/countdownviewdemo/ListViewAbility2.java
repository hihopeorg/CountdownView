package cn.iwgang.countdownviewdemo;


import cn.iwgang.countdownview.CountdownView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.utils.PlainArray;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/*
     此类模拟在ListContainer中使用倒计时
     方案二：自己维护倒计时任何，再调用countdownView.updateShow来刷新显示
 */
public class ListViewAbility2 extends Ability {
    private List<ItemInfo> mDataList;
    private MyListAdapter mMyAdapter;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_listview);

        initData();

        ListContainer lvList = (ListContainer) findComponentById(ResourceTable.Id_lv_list);
        lvList.setItemProvider(mMyAdapter = new MyListAdapter(this, mDataList));
    }

    private void initData() {
        mDataList = new ArrayList<>();

        for (int i = 1; i < 20; i++) {
            mDataList.add(new ItemInfo(1000 + i, "ListView_测试标题_" + i, i * 20 * 1000));
        }

        // 校对倒计时
        long curTime = System.currentTimeMillis();
        for (ItemInfo itemInfo : mDataList) {
            itemInfo.setEndTime(curTime + itemInfo.getCountdown());
        }
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        if (null != mMyAdapter) {
            mMyAdapter.startRefreshTime();
        }
    }

    @Override
    protected void onBackground() {
        super.onBackground();
        if (null != mMyAdapter) {
            mMyAdapter.cancelRefreshTime();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (null != mMyAdapter) {
            mMyAdapter.cancelRefreshTime();
        }
    }

    static class MyListAdapter extends BaseItemProvider {
        private Context mContext;
        private List<ItemInfo> mDatas;
        private final PlainArray<MyViewHolder> mCountdownVHList;
        private EventHandler mHandler = new EventHandler(EventRunner.create());
        private Timer mTimer;
        private boolean isCancel = true;

        public MyListAdapter(Context context, List<ItemInfo> datas) {
            this.mContext = context;
            this.mDatas = datas;
            mCountdownVHList = new PlainArray<>();
            startRefreshTime();
        }

        public void startRefreshTime() {
            if (!isCancel) return;

            if (null != mTimer) {
                mTimer.cancel();
            }

            isCancel = false;
            mTimer = new Timer();
            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    mHandler.postTask(mRefreshTimeRunnable);
                }
            }, 0, 10);
        }

        public void cancelRefreshTime() {
            isCancel = true;
            if (null != mTimer) {
                mTimer.cancel();
            }
            mHandler.removeTask(mRefreshTimeRunnable);
        }

        @Override
        public int getCount() {
            return mDatas.size();
        }

        @Override
        public Object getItem(int position) {
            return mDatas.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Component getComponent(int position, Component convertView, ComponentContainer parent) {
            MyViewHolder holder;
            if (convertView == null) {
                convertView = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_list_item, parent, false);
                holder = new MyViewHolder();
                holder.initView(convertView);
                convertView.setTag(holder);
            } else {
                holder = (MyViewHolder) convertView.getTag();
            }

            ItemInfo curItemInfo = mDatas.get(position);
            holder.bindData(curItemInfo);

            // 处理倒计时
            if (curItemInfo.getCountdown() > 0) {
                synchronized (mCountdownVHList) {
                    mCountdownVHList.put(curItemInfo.getId(), holder);
                }
            }

            return convertView;
        }

        private Runnable mRefreshTimeRunnable = new Runnable() {
            @Override
            public void run() {
                if (mCountdownVHList.size() == 0) return;

                synchronized (mCountdownVHList) {
                    long currentTime = System.currentTimeMillis();
                    int key;
                    for (int i = 0; i < mCountdownVHList.size(); i++) {
                        key = mCountdownVHList.keyAt(i);
                        MyViewHolder curMyViewHolder = mCountdownVHList.get(key).get();
                        if (currentTime >= curMyViewHolder.getBean().getEndTime()) {
                            // 倒计时结束
                            curMyViewHolder.getBean().setCountdown(0);
                            mCountdownVHList.remove(key);
                            notifyDataChanged();
                        } else {
                            curMyViewHolder.refreshTime(currentTime);
                        }

                    }
                }
            }
        };

        static class MyViewHolder {
            private Text mTvTitle;
            private CountdownView mCvCountdownView;
            private ItemInfo mItemInfo;

            public void initView(Component convertView) {
                mTvTitle = (Text) convertView.findComponentById(ResourceTable.Id_tv_title);
                mCvCountdownView = (CountdownView) convertView.findComponentById(ResourceTable.Id_cv_countdownView);
            }

            public void bindData(ItemInfo itemInfo) {
                mItemInfo = itemInfo;

                if (itemInfo.getCountdown() > 0) {
                    refreshTime(System.currentTimeMillis());
                } else {
                    mCvCountdownView.allShowZero();
                }

                mTvTitle.setText(itemInfo.getTitle());
            }

            public void refreshTime(long curTimeMillis) {
                if (null == mItemInfo || mItemInfo.getCountdown() <= 0) return;

                mCvCountdownView.updateShow(mItemInfo.getEndTime() - curTimeMillis);
            }

            public ItemInfo getBean() {
                return mItemInfo;
            }
        }
    }

    static class ItemInfo {
        private int id;
        private String title;
        private long countdown;
        /*
           根据服务器返回的countdown换算成手机对应的开奖时间 (毫秒)
           [正常情况最好由服务器返回countdown字段，然后客户端再校对成该手机对应的时间，不然误差很大]
         */
        private long endTime;

        public ItemInfo(int id, String title, long countdown) {
            this.id = id;
            this.title = title;
            this.countdown = countdown;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public long getCountdown() {
            return countdown;
        }

        public void setCountdown(long countdown) {
            this.countdown = countdown;
        }

        public long getEndTime() {
            return endTime;
        }

        public void setEndTime(long endTime) {
            this.endTime = endTime;
        }
    }

}