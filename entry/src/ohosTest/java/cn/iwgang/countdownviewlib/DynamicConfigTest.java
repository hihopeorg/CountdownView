package cn.iwgang.countdownviewlib;

import cn.iwgang.countdownview.DynamicConfig;
import org.junit.Assert;
import org.junit.Test;

public class DynamicConfigTest {

    @Test
    public void getTimeTextSize() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setTimeTextSize(100).build();
        Float result = builder.build().getTimeTextSize();
        Assert.assertTrue(result == 100);
    }

    @Test
    public void getTimeTextColor() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setTimeTextColor(100).build();
        Integer result = builder.build().getTimeTextColor();
        Assert.assertTrue(result == 100);
    }

    @Test
    public void isTimeTextBold() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setTimeTextBold(true).build();
        Boolean result = builder.build().isTimeTextBold();
        Assert.assertTrue(result);
    }

    @Test
    public void getSuffixTextSize() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setSuffixTextSize(100).build();
        Float result = builder.build().getSuffixTextSize();
        Assert.assertTrue(result == 100);
    }

    @Test
    public void getSuffixTextColor() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setSuffixTextColor(100).build();
        Integer result = builder.build().getSuffixTextColor();
        Assert.assertTrue(result == 100);
    }

    @Test
    public void isSuffixTimeTextBold() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setSuffixTextBold(true).build();
        Boolean result = builder.build().isSuffixTimeTextBold();
        Assert.assertTrue(result);
    }

    @Test
    public void getSuffix() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setSuffix(";").build();
        String result = builder.build().getSuffix();
        Assert.assertTrue(result.equals(";"));
    }

    @Test
    public void getSuffixDay() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setSuffixDay(";").build();
        String result = builder.build().getSuffixDay();
        Assert.assertTrue(result.equals(";"));
    }

    @Test
    public void getSuffixHour() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setSuffixHour(";").build();
        String result = builder.build().getSuffixHour();
        Assert.assertTrue(result.equals(";"));
    }

    @Test
    public void getSuffixMinute() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setSuffixMinute(";").build();
        String result = builder.build().getSuffixMinute();
        Assert.assertTrue(result.equals(";"));
    }

    @Test
    public void getSuffixSecond() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setSuffixSecond(";").build();
        String result = builder.build().getSuffixSecond();
        Assert.assertTrue(result.equals(";"));
    }

    @Test
    public void getSuffixMillisecond() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setSuffixMillisecond(";").build();
        String result = builder.build().getSuffixMillisecond();
        Assert.assertTrue(result.equals(";"));
    }

    @Test
    public void getSuffixGravity() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setSuffixGravity("top").build();
        String result = builder.build().getSuffixGravity();
        Assert.assertTrue(result.equals("top"));
    }

    @Test
    public void getSuffixLRMargin() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setSuffixLRMargin(100).build();
        Float result = builder.build().getSuffixLRMargin();
        Assert.assertTrue(result == 100);
    }

    @Test
    public void getSuffixDayLeftMargin() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setSuffixDayLeftMargin(100).build();
        Float result = builder.build().getSuffixDayLeftMargin();
        Assert.assertTrue(result == 100);
    }

    @Test
    public void getSuffixDayRightMargin() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setSuffixDayRightMargin(100).build();
        Float result = builder.build().getSuffixDayRightMargin();
        Assert.assertTrue(result == 100);
    }

    @Test
    public void getSuffixHourLeftMargin() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setSuffixHourLeftMargin(100).build();
        Float result = builder.build().getSuffixHourLeftMargin();
        Assert.assertTrue(result == 100);
    }

    @Test
    public void getSuffixHourRightMargin() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setSuffixHourRightMargin(100).build();
        Float result = builder.build().getSuffixHourRightMargin();
        Assert.assertTrue(result == 100);
    }

    @Test
    public void getSuffixMinuteLeftMargin() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setSuffixMinuteLeftMargin(100).build();
        Float result = builder.build().getSuffixMinuteLeftMargin();
        Assert.assertTrue(result == 100);
    }

    @Test
    public void getSuffixMinuteRightMargin() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setSuffixMinuteRightMargin(100).build();
        Float result = builder.build().getSuffixMinuteRightMargin();
        Assert.assertTrue(result == 100);
    }

    @Test
    public void getSuffixSecondLeftMargin() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setSuffixSecondLeftMargin(100).build();
        Float result = builder.build().getSuffixSecondLeftMargin();
        Assert.assertTrue(result == 100);
    }

    @Test
    public void getSuffixSecondRightMargin() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setSuffixSecondRightMargin(100).build();
        Float result = builder.build().getSuffixSecondRightMargin();
        Assert.assertTrue(result == 100);
    }

    @Test
    public void getSuffixMillisecondLeftMargin() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setSuffixMillisecondLeftMargin(100).build();
        Float result = builder.build().getSuffixMillisecondLeftMargin();
        Assert.assertTrue(result == 100);
    }

    @Test
    public void isConvertDaysToHours() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setConvertDaysToHours(true).build();
        Boolean result = builder.build().isConvertDaysToHours();
        Assert.assertTrue(result);
    }

    @Test
    public void isShowDay() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setShowDay(true).build();
        Boolean result = builder.build().isShowDay();
        Assert.assertTrue(result);
    }

    @Test
    public void isShowHour() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setShowHour(true).build();
        Boolean result = builder.build().isShowHour();
        Assert.assertTrue(result);
    }

    @Test
    public void isShowMinute() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setShowMinute(true).build();
        Boolean result = builder.build().isShowMinute();
        Assert.assertTrue(result);
    }

    @Test
    public void isShowSecond() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setShowSecond(true).build();
        Boolean result = builder.build().isShowSecond();
        Assert.assertTrue(result);
    }

    @Test
    public void isShowMillisecond() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        builder.setShowMillisecond(true).build();
        Boolean result = builder.build().isShowMillisecond();
        Assert.assertTrue(result);
    }

    @Test
    public void getBackgroundInfo() {
        DynamicConfig.Builder builder = new DynamicConfig.Builder();
        DynamicConfig.BackgroundInfo backgroundInfo = new DynamicConfig.BackgroundInfo();
        backgroundInfo.setColor(100);
        builder.setBackgroundInfo(backgroundInfo).build();
        Object result = builder.build().getBackgroundInfo();
        Assert.assertNotNull(result);
    }

}
