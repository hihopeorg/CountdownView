package cn.iwgang.countdownviewlib;

import cn.iwgang.countdownview.BaseCountdown;
import cn.iwgang.countdownview.Utils;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.Assert;
import org.junit.Test;

public class BaseCountdownTest {

    @Test
    public void getisSuffixTextBold() {
        BaseCountdown baseCountdown = new BaseCountdown();
        baseCountdown.initPaint();
        baseCountdown.setSuffixTextBold(true);
        Assert.assertTrue(baseCountdown.getisSuffixTextBold());
    }

    @Test
    public void getmSuffixGravity() {
        BaseCountdown baseCountdown = new BaseCountdown();
        baseCountdown.initPaint();
        baseCountdown.setSuffixGravity("A");
        Assert.assertTrue("A".equals(baseCountdown.getmSuffixGravity()));
    }

    @Test
    public void getmSuffixLRMargin() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        BaseCountdown baseCountdown = new BaseCountdown();
        baseCountdown.initPaint();
        baseCountdown.setSuffixLRMargin(10);
        Assert.assertTrue(Utils.vp2px(context, 10) == baseCountdown.getmSuffixLRMargin());
    }

    @Test
    public void getmSuffix() {
        BaseCountdown baseCountdown = new BaseCountdown();
        baseCountdown.initPaint();
        baseCountdown.setSuffix("A");
        Assert.assertTrue("A".equals(baseCountdown.getmSuffix()));
    }

    @Test
    public void getmSuffixTextColor() {
        BaseCountdown baseCountdown = new BaseCountdown();
        baseCountdown.initPaint();
        baseCountdown.setSuffixTextColor(100);
        Assert.assertTrue(100 == baseCountdown.getmSuffixTextColor());
    }

    @Test
    public void getmSuffixTextSize() {
        BaseCountdown baseCountdown = new BaseCountdown();
        baseCountdown.initPaint();
        baseCountdown.setSuffixTextSize(100);
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Assert.assertTrue(Utils.fp2px(context, 100) == baseCountdown.getmSuffixTextSize());
    }

    @Test
    public void getisTimeTextBold() {
        BaseCountdown baseCountdown = new BaseCountdown();
        baseCountdown.initPaint();
        baseCountdown.setTimeTextBold(true);
        Assert.assertTrue(baseCountdown.getisTimeTextBold());
    }

    @Test
    public void getmTimeTextColor() {
        BaseCountdown baseCountdown = new BaseCountdown();
        baseCountdown.initPaint();
        baseCountdown.setTimeTextColor(100);
        Assert.assertTrue(100 == baseCountdown.getmTimeTextColor());
    }

    @Test
    public void getmTimeTextSize() {
        BaseCountdown baseCountdown = new BaseCountdown();
        baseCountdown.initPaint();
        baseCountdown.setTimeTextSize(100);
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Assert.assertTrue(Utils.fp2px(context, 100) == baseCountdown.getmTimeTextSize());
    }

}
