package cn.iwgang.countdownviewlib;

import cn.iwgang.countdownview.BackgroundCountdown;
import cn.iwgang.countdownview.Utils;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.Assert;
import org.junit.Test;

public class BackgroundCountdownTest {

    @Test
    public void getTimeBgRadius() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        BackgroundCountdown backgroundCountdown = new BackgroundCountdown();
        backgroundCountdown.setContext(context);
        backgroundCountdown.setTimeBgRadius(10);
        float radius = backgroundCountdown.getTimeBgRadius();
        Assert.assertTrue("test failed", Utils.vp2px(context, 10) == radius);
    }

    @Test
    public void getIsShowTimeBgDivisionLine() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        BackgroundCountdown backgroundCountdown = new BackgroundCountdown();
        backgroundCountdown.setContext(context);
        backgroundCountdown.setIsShowTimeBgDivisionLine(true);
        boolean boo = backgroundCountdown.getIsShowTimeBgDivisionLine();
        Assert.assertTrue("test failed", boo);
    }

    @Test
    public void getTimeBgDivisionLineColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        BackgroundCountdown backgroundCountdown = new BackgroundCountdown();
        backgroundCountdown.setContext(context);
        backgroundCountdown.setTimeBgDivisionLineColor(-66345);
        float color = backgroundCountdown.getTimeBgDivisionLineColor();
        Assert.assertTrue("test failed", -66345 == color);
    }

    @Test
    public void getIsShowTimeBgBorder() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        BackgroundCountdown backgroundCountdown = new BackgroundCountdown();
        backgroundCountdown.setContext(context);
        backgroundCountdown.setIsShowTimeBgBorder(true);
        boolean boo = backgroundCountdown.getIsShowTimeBgBorder();
        Assert.assertTrue("test failed", boo);
    }

    @Test
    public void getmTimeBgBorderColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        BackgroundCountdown backgroundCountdown = new BackgroundCountdown();
        backgroundCountdown.setContext(context);
        backgroundCountdown.setTimeBgBorderColor(-66345);
        float color = backgroundCountdown.getmTimeBgBorderColor();
        Assert.assertTrue("test failed", -66345 == color);
    }

    @Test
    public void getmTimeBgBorderSize() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        BackgroundCountdown backgroundCountdown = new BackgroundCountdown();
        backgroundCountdown.setContext(context);
        backgroundCountdown.setTimeBgBorderSize(10);
        float radius = backgroundCountdown.getmTimeBgBorderSize();
        Assert.assertTrue("test failed", Utils.vp2px(context, 10) == radius);
    }

    @Test
    public void getmTimeBgBorderRadius() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        BackgroundCountdown backgroundCountdown = new BackgroundCountdown();
        backgroundCountdown.setContext(context);
        backgroundCountdown.setTimeBgBorderRadius(10);
        float radius = backgroundCountdown.getmTimeBgBorderRadius();
        Assert.assertTrue("test failed", Utils.vp2px(context, 10) == radius);
    }
}