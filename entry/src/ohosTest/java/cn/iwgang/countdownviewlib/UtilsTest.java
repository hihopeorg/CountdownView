package cn.iwgang.countdownviewlib;

import cn.iwgang.countdownview.Utils;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.Assert;
import org.junit.Test;

public class UtilsTest {

    @Test
    public void vp2px() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Assert.assertTrue(300 == Utils.vp2px(context, 100));
    }

    @Test
    public void fp2px() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Assert.assertTrue(300 == Utils.fp2px(context, 100));
    }

    @Test
    public void formatNum() {
        Assert.assertTrue("01".equals(Utils.formatNum(1)));
        Assert.assertTrue("11".equals(Utils.formatNum(11)));
    }

    @Test
    public void formatMillisecond() {
        Assert.assertTrue("01".equals(Utils.formatMillisecond(1)));
        Assert.assertTrue("11".equals(Utils.formatMillisecond(11)));
        Assert.assertTrue("11".equals(Utils.formatMillisecond(111)));
    }

}
