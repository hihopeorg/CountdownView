package cn.iwgang.countdownviewdemo;

import cn.iwgang.countdownview.BackgroundCountdown;
import cn.iwgang.countdownview.CountdownView;
import cn.iwgang.countdownview.DynamicConfig;
import cn.iwgang.countdownviewdemo.utils.EventHelper;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Checkbox;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleOhosTest {

    @After
    public void tearDown() {
        EventHelper.clearAbilities();
        sleep();
    }

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("cn.iwgang.countdownviewdemo", actualBundleName);
        sleep();
    }

    @Test
    public void showMainPage() {
        Ability ability = EventHelper.startAbility(MainAbility.class);
        assertNotNull(ability);
        sleep();
    }

    @Test
    public void testBgRadiusSize() {
        Ability ability = EventHelper.startAbility(DynamicShowAbility.class);
        sleep();
        assertNotNull(ability);
        CountdownView mCvCountdownViewTestHasBg = (CountdownView) ability.findComponentById(ResourceTable.Id_cv_countdownViewTestHasBg);
        BackgroundCountdown backgroundCountdown = (BackgroundCountdown) mCvCountdownViewTestHasBg.getmCountdown();
        float size = backgroundCountdown.getTimeBgRadius();

        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgRadiusPlus));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgRadiusPlus));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgRadiusPlus));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgRadiusPlus));
        sleep();
        assertFalse(size == backgroundCountdown.getTimeBgRadius());

        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgRadiusSubtract));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgRadiusSubtract));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgRadiusSubtract));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgRadiusSubtract));
        sleep();
        assertTrue(size == backgroundCountdown.getTimeBgRadius());
        sleep();
    }


    @Test
    public void showOrHideBg() {
        Ability ability = EventHelper.startAbility(DynamicShowAbility.class);
        assertNotNull(ability);
        sleep();
        CountdownView mCvCountdownViewTest = (CountdownView) ability.findComponentById(ResourceTable.Id_cv_countdownViewTest);
        CountdownView mCvCountdownViewTestHasBg = (CountdownView) ability.findComponentById(ResourceTable.Id_cv_countdownViewTestHasBg);
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_cb_hasBackground));
        sleep();
        assertTrue(mCvCountdownViewTestHasBg.getVisibility() == Component.HIDE);
        assertTrue(mCvCountdownViewTest.getVisibility() == Component.VISIBLE);
        sleep();
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_cb_hasBackground));
        sleep();
        assertTrue(mCvCountdownViewTestHasBg.getVisibility() == Component.VISIBLE);
        assertTrue(mCvCountdownViewTest.getVisibility() == Component.HIDE);
        sleep();
    }

    @Test
    public void testTextSize() {
        Ability ability = EventHelper.startAbility(DynamicShowAbility.class);
        assertNotNull(ability);
        sleep();
        CountdownView mCvCountdownViewTestHasBg = (CountdownView) ability.findComponentById(ResourceTable.Id_cv_countdownViewTestHasBg);
        float testSize = mCvCountdownViewTestHasBg.getmCountdown().getmTimeTextSize();
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_timeTextSizePlus));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_timeTextSizePlus));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_timeTextSizePlus));
        assertTrue(testSize != mCvCountdownViewTestHasBg.getmCountdown().getmTimeTextSize());
        sleep();
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_timeTextSizeSubtract));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_timeTextSizeSubtract));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_timeTextSizeSubtract));
        assertTrue(testSize == mCvCountdownViewTestHasBg.getmCountdown().getmTimeTextSize());
        sleep();
    }

    @Test
    public void testSuffixTextSize() {
        Ability ability = EventHelper.startAbility(DynamicShowAbility.class);
        assertNotNull(ability);
        sleep();
        CountdownView mCvCountdownViewTestHasBg = (CountdownView) ability.findComponentById(ResourceTable.Id_cv_countdownViewTestHasBg);
        float testSize = mCvCountdownViewTestHasBg.getmCountdown().getmSuffixTextSize();
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_suffixTextSizePlus));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_suffixTextSizePlus));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_suffixTextSizePlus));
        assertTrue(testSize != mCvCountdownViewTestHasBg.getmCountdown().getmSuffixTextSize());
        sleep();
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_suffixTextSizeSubtract));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_suffixTextSizeSubtract));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_suffixTextSizeSubtract));
        assertTrue(testSize == mCvCountdownViewTestHasBg.getmCountdown().getmSuffixTextSize());
        sleep();
    }

    @Test
    public void testSuffixGravity() {
        Ability ability = EventHelper.startAbility(DynamicShowAbility.class);
        assertNotNull(ability);
        sleep();
        CountdownView mCvCountdownViewTestHasBg = (CountdownView) ability.findComponentById(ResourceTable.Id_cv_countdownViewTestHasBg);
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_suffixGravityTop));
        assertTrue(mCvCountdownViewTestHasBg.getmCountdown().getmSuffixGravity().equals("top"));
        sleep();
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_suffixGravityCenter));
        assertTrue(mCvCountdownViewTestHasBg.getmCountdown().getmSuffixGravity().equals("center"));
        sleep();
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_suffixGravityBottom));
        assertTrue(mCvCountdownViewTestHasBg.getmCountdown().getmSuffixGravity().equals("bottom"));
        sleep();
    }

    @Test
    public void testTimeColor() {
        Ability ability = EventHelper.startAbility(DynamicShowAbility.class);
        assertNotNull(ability);
        sleep();
        CountdownView mCvCountdownViewTestHasBg = (CountdownView) ability.findComponentById(ResourceTable.Id_cv_countdownViewTestHasBg);
        DynamicConfig dynamicConfig = new DynamicConfig.Builder().setTimeTextColor(Color.RED.getValue()).build();
        mCvCountdownViewTestHasBg.dynamicShow(dynamicConfig);
        sleep();
        int color = mCvCountdownViewTestHasBg.getmCountdown().getmTimeTextColor();
        assertTrue(color == Color.RED.getValue());
        sleep();
    }

    @Test
    public void testSuffixColor() {
        Ability ability = EventHelper.startAbility(DynamicShowAbility.class);
        assertNotNull(ability);
        sleep();
        CountdownView mCvCountdownViewTestHasBg = (CountdownView) ability.findComponentById(ResourceTable.Id_cv_countdownViewTestHasBg);
        DynamicConfig dynamicConfig = new DynamicConfig.Builder().setSuffixTextColor(Color.RED.getValue()).build();
        mCvCountdownViewTestHasBg.dynamicShow(dynamicConfig);
        sleep();
        int color = mCvCountdownViewTestHasBg.getmCountdown().getmSuffixTextColor();
        assertTrue(color == Color.RED.getValue());
        sleep();
    }

    @Test
    public void testTimeShowConfigure() {
        Ability ability = EventHelper.startAbility(DynamicShowAbility.class);
        assertNotNull(ability);
        sleep();
        CountdownView mCvCountdownViewTestHasBg = (CountdownView) ability.findComponentById(ResourceTable.Id_cv_countdownViewTestHasBg);
        Checkbox cbDay = (Checkbox) ability.findComponentById(ResourceTable.Id_cb_day);
        Checkbox cbHour = (Checkbox) ability.findComponentById(ResourceTable.Id_cb_hour);
        Checkbox cbMinute = (Checkbox) ability.findComponentById(ResourceTable.Id_cb_minute);
        Checkbox cbSecond = (Checkbox) ability.findComponentById(ResourceTable.Id_cb_second);
        Checkbox cbMillisecond = (Checkbox) ability.findComponentById(ResourceTable.Id_cb_millisecond);
        cbDay.setChecked(false);
        cbHour.setChecked(false);
        cbMinute.setChecked(false);
        sleep();
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_refTimeShow));
        sleep();
        assertFalse(mCvCountdownViewTestHasBg.getmCountdown().isShowDay);
        assertFalse(mCvCountdownViewTestHasBg.getmCountdown().isShowHour);
        assertFalse(mCvCountdownViewTestHasBg.getmCountdown().isShowMinute);
        sleep();
    }

    @Test
    public void testSuffix() {
        Ability ability = EventHelper.startAbility(DynamicShowAbility.class);
        assertNotNull(ability);
        sleep();
        CountdownView mCvCountdownViewTestHasBg = (CountdownView) ability.findComponentById(ResourceTable.Id_cv_countdownViewTestHasBg);
        final TextField etSuffixDay = (TextField) ability.findComponentById(ResourceTable.Id_et_suffixDay);
        final TextField etSuffixHour = (TextField) ability.findComponentById(ResourceTable.Id_et_suffixHour);
        final TextField etSuffixMinute = (TextField) ability.findComponentById(ResourceTable.Id_et_suffixMinute);
        final TextField etSuffixSecond = (TextField) ability.findComponentById(ResourceTable.Id_et_suffixSecond);
        final TextField etSuffixMillisecond = (TextField) ability.findComponentById(ResourceTable.Id_et_suffixMillisecond);

        new EventHandler(EventRunner.getMainEventRunner()).postTask(() -> {
            etSuffixDay.setText("天");
            etSuffixHour.setText("时");
            etSuffixMinute.setText("分");
            etSuffixSecond.setText("秒");
            etSuffixMillisecond.setText("毫秒");
        });

        sleep();
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btm_refSuffix));
        sleep();
        assertTrue(mCvCountdownViewTestHasBg.getmCountdown().getmSuffixDay().equals("天"));
        assertTrue(mCvCountdownViewTestHasBg.getmCountdown().getmSuffixHour().equals("时"));
        assertTrue(mCvCountdownViewTestHasBg.getmCountdown().getmSuffixMinute().equals("分"));
        assertTrue(mCvCountdownViewTestHasBg.getmCountdown().getmSuffixSecond().equals("秒"));
        assertTrue(mCvCountdownViewTestHasBg.getmCountdown().getmSuffixMillisecond().equals("毫秒"));
        sleep();
    }

    @Test
    public void testSwitchBg() {
        Ability ability = EventHelper.startAbility(DynamicShowAbility.class);
        assertNotNull(ability);
        sleep();
        CountdownView mCvCountdownViewTestHasBg = (CountdownView) ability.findComponentById(ResourceTable.Id_cv_countdownViewTestHasBg);
        int bgColor1 = ((BackgroundCountdown) mCvCountdownViewTestHasBg.getmCountdown()).getmTimeBgColor();
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_theme1));
        sleep();
        assertTrue(bgColor1 != ((BackgroundCountdown) mCvCountdownViewTestHasBg.getmCountdown()).getmTimeBgColor());
        sleep();
        int bgColor2 = ((BackgroundCountdown) mCvCountdownViewTestHasBg.getmCountdown()).getmTimeBgColor();
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_theme2));
        sleep();
        assertTrue(bgColor2 != ((BackgroundCountdown) mCvCountdownViewTestHasBg.getmCountdown()).getmTimeBgColor());
        sleep();
        int bgColor3 = ((BackgroundCountdown) mCvCountdownViewTestHasBg.getmCountdown()).getmTimeBgColor();
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_theme3));
        sleep();
        assertTrue(bgColor3 != ((BackgroundCountdown) mCvCountdownViewTestHasBg.getmCountdown()).getmTimeBgColor());
        sleep();
    }

    @Test
    public void testBgSize() {
        Ability ability = EventHelper.startAbility(DynamicShowAbility.class);
        assertNotNull(ability);
        sleep();
        CountdownView mCvCountdownViewTestHasBg = (CountdownView) ability.findComponentById(ResourceTable.Id_cv_countdownViewTestHasBg);
        BackgroundCountdown backgroundCountdown = (BackgroundCountdown) mCvCountdownViewTestHasBg.getmCountdown();
        float size = backgroundCountdown.getmTimeBgSize();

        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgSizePlus));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgSizePlus));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgSizePlus));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgSizePlus));
        sleep();
        assertFalse(size == backgroundCountdown.getmTimeBgSize());

        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgSizeSubtract));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgSizeSubtract));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgSizeSubtract));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgSizeSubtract));
        sleep();
        assertTrue(size == backgroundCountdown.getmTimeBgSize());
        sleep();
    }


    @Test
    public void testBgColor() {
        Ability ability = EventHelper.startAbility(DynamicShowAbility.class);
        assertNotNull(ability);
        sleep();
        CountdownView mCvCountdownViewTestHasBg = (CountdownView) ability.findComponentById(ResourceTable.Id_cv_countdownViewTestHasBg);
        DynamicConfig.BackgroundInfo info = new DynamicConfig.BackgroundInfo();
        info.setColor(Color.RED.getValue());
        DynamicConfig dynamicConfig = new DynamicConfig.Builder().setBackgroundInfo(info).build();
        mCvCountdownViewTestHasBg.dynamicShow(dynamicConfig);
        sleep();
        BackgroundCountdown backgroundCountdown = (BackgroundCountdown) mCvCountdownViewTestHasBg.getmCountdown();
        int color = backgroundCountdown.getmTimeBgColor();
        assertTrue(color == Color.RED.getValue());
        sleep();
    }

    @Test
    public void testDividerColor() {
        Ability ability = EventHelper.startAbility(DynamicShowAbility.class);
        assertNotNull(ability);
        sleep();
        CountdownView mCvCountdownViewTestHasBg = (CountdownView) ability.findComponentById(ResourceTable.Id_cv_countdownViewTestHasBg);
        DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
        dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setShowTimeBgDivisionLine(true).setDivisionLineColor(Color.RED.getValue()));
        mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
        sleep();
        BackgroundCountdown backgroundCountdown = (BackgroundCountdown) mCvCountdownViewTestHasBg.getmCountdown();
        int color = backgroundCountdown.getTimeBgDivisionLineColor();
        assertTrue(color == Color.RED.getValue());
        sleep();
    }

    @Test
    public void testBorderColor() {
        Ability ability = EventHelper.startAbility(DynamicShowAbility.class);
        assertNotNull(ability);
        sleep();
        CountdownView mCvCountdownViewTestHasBg = (CountdownView) ability.findComponentById(ResourceTable.Id_cv_countdownViewTestHasBg);
        DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
        dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo()
                .setBorderSize(10F)
                .setShowTimeBgBorder(true)
                .setBorderColor(Color.RED.getValue())
        );
        mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
        sleep();
        BackgroundCountdown backgroundCountdown = (BackgroundCountdown) mCvCountdownViewTestHasBg.getmCountdown();
        int color = backgroundCountdown.getmTimeBgBorderColor();
        assertTrue(color == Color.RED.getValue());
        sleep();
    }

    @Test
    public void testBorderSize() {
        Ability ability = EventHelper.startAbility(DynamicShowAbility.class);
        assertNotNull(ability);
        sleep();
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_cb_bgBorder));
        CountdownView mCvCountdownViewTestHasBg = (CountdownView) ability.findComponentById(ResourceTable.Id_cv_countdownViewTestHasBg);
        DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
        dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo()
                .setBorderColor(Color.RED.getValue())
        );
        mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
        sleep();

        BackgroundCountdown backgroundCountdown = (BackgroundCountdown) mCvCountdownViewTestHasBg.getmCountdown();
        float size = backgroundCountdown.getmTimeBgBorderSize();

        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgBorderSizePlus));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgBorderSizePlus));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgBorderSizePlus));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgBorderSizePlus));
        sleep();
        assertFalse(size == backgroundCountdown.getmTimeBgBorderSize());

        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgBorderSizeSubtract));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgBorderSizeSubtract));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgBorderSizeSubtract));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgBorderSizeSubtract));
        sleep();
        assertTrue(size == backgroundCountdown.getmTimeBgBorderSize());
        sleep();
    }

    @Test
    public void testBorderRadius() {
        Ability ability = EventHelper.startAbility(DynamicShowAbility.class);
        assertNotNull(ability);
        sleep();
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_cb_bgBorder));
        CountdownView mCvCountdownViewTestHasBg = (CountdownView) ability.findComponentById(ResourceTable.Id_cv_countdownViewTestHasBg);
        DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
        dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo()
                .setBorderColor(Color.RED.getValue())
        );
        mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
        sleep();

        BackgroundCountdown backgroundCountdown = (BackgroundCountdown) mCvCountdownViewTestHasBg.getmCountdown();
        float size = backgroundCountdown.getmTimeBgBorderRadius();

        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgBorderRadiusPlus));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgBorderRadiusPlus));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgBorderRadiusPlus));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgBorderRadiusPlus));
        sleep();
        assertFalse(size == backgroundCountdown.getmTimeBgBorderRadius());

        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgBorderRadiusSubtract));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgBorderRadiusSubtract));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgBorderRadiusSubtract));
        EventHelper.triggerClickEvent(ability, ability.findComponentById(ResourceTable.Id_btn_bgBorderRadiusSubtract));
        sleep();
        assertTrue(size == backgroundCountdown.getmTimeBgBorderRadius());
        sleep();
    }

    @Test
    public void testGoToListPage(){
        Ability ability = EventHelper.startAbility(ListViewAbility.class);
        assertNotNull(ability);
        sleep();
    }

    private void sleep() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}